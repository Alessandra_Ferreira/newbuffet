﻿namespace Buffet.Telas
{
    partial class EditarEstoque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.cbofornecedores = new System.Windows.Forms.ComboBox();
            this.ID_Festa = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtProduto = new System.Windows.Forms.TextBox();
            this.nudsaida = new System.Windows.Forms.DomainUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.btnalterar = new System.Windows.Forms.PictureBox();
            this.label28 = new System.Windows.Forms.Label();
            this.btnsalvar = new System.Windows.Forms.PictureBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btncancelar = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtdtValidade = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtatual = new System.Windows.Forms.MaskedTextBox();
            this.txtsaida = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nudatual = new System.Windows.Forms.DomainUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.nudEntrega = new System.Windows.Forms.DomainUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtdtEntrega = new System.Windows.Forms.MaskedTextBox();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnalterar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnsalvar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btncancelar)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.cbofornecedores);
            this.panel3.Controls.Add(this.ID_Festa);
            this.panel3.Location = new System.Drawing.Point(0, 23);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(629, 59);
            this.panel3.TabIndex = 198;
            // 
            // cbofornecedores
            // 
            this.cbofornecedores.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbofornecedores.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbofornecedores.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.cbofornecedores.FormattingEnabled = true;
            this.cbofornecedores.Location = new System.Drawing.Point(128, 17);
            this.cbofornecedores.Name = "cbofornecedores";
            this.cbofornecedores.Size = new System.Drawing.Size(419, 28);
            this.cbofornecedores.TabIndex = 200;
            // 
            // ID_Festa
            // 
            this.ID_Festa.AutoSize = true;
            this.ID_Festa.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ID_Festa.Location = new System.Drawing.Point(19, 21);
            this.ID_Festa.Name = "ID_Festa";
            this.ID_Festa.Size = new System.Drawing.Size(103, 21);
            this.ID_Festa.TabIndex = 194;
            this.ID_Festa.Text = "Fornecedor:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(620, 36);
            this.panel2.TabIndex = 199;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(559, 6);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(24, 27);
            this.label21.TabIndex = 255;
            this.label21.Text = "_";
            this.label21.Click += new System.EventHandler(this.label21_Click_1);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(589, 8);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 23);
            this.label25.TabIndex = 254;
            this.label25.Text = "X";
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(8, 9);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(260, 21);
            this.label20.TabIndex = 49;
            this.label20.Text = "Magic Buffet - Atualizar Estoque";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(133, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(95, 24);
            this.label15.TabIndex = 217;
            this.label15.Text = "Produto:";
            // 
            // txtProduto
            // 
            this.txtProduto.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProduto.Location = new System.Drawing.Point(234, 11);
            this.txtProduto.Name = "txtProduto";
            this.txtProduto.Size = new System.Drawing.Size(313, 26);
            this.txtProduto.TabIndex = 216;
            // 
            // nudsaida
            // 
            this.nudsaida.BackColor = System.Drawing.Color.WhiteSmoke;
            this.nudsaida.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudsaida.Location = new System.Drawing.Point(501, 33);
            this.nudsaida.Name = "nudsaida";
            this.nudsaida.Size = new System.Drawing.Size(46, 26);
            this.nudsaida.TabIndex = 233;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(399, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 21);
            this.label8.TabIndex = 232;
            this.label8.Text = "Qtd Saida:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(280, 403);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 17);
            this.label26.TabIndex = 242;
            this.label26.Text = "Alterar";
            // 
            // btnalterar
            // 
            this.btnalterar.Image = global::Buffet.Properties.Resources.alterar;
            this.btnalterar.Location = new System.Drawing.Point(275, 367);
            this.btnalterar.Name = "btnalterar";
            this.btnalterar.Size = new System.Drawing.Size(58, 33);
            this.btnalterar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnalterar.TabIndex = 239;
            this.btnalterar.TabStop = false;
            this.btnalterar.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(46, 403);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(46, 17);
            this.label28.TabIndex = 240;
            this.label28.Text = "Salvar";
            // 
            // btnsalvar
            // 
            this.btnsalvar.Image = global::Buffet.Properties.Resources._678134_sign_check_512;
            this.btnsalvar.Location = new System.Drawing.Point(40, 367);
            this.btnsalvar.Name = "btnsalvar";
            this.btnsalvar.Size = new System.Drawing.Size(58, 33);
            this.btnsalvar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnsalvar.TabIndex = 238;
            this.btnsalvar.TabStop = false;
            this.btnsalvar.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(517, 403);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(63, 17);
            this.label27.TabIndex = 244;
            this.label27.Text = "Cancelar";
            // 
            // btncancelar
            // 
            this.btncancelar.Image = global::Buffet.Properties.Resources._1024px_Deletion_icon_svg;
            this.btncancelar.Location = new System.Drawing.Point(519, 367);
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.Size = new System.Drawing.Size(58, 33);
            this.btncancelar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btncancelar.TabIndex = 243;
            this.btncancelar.TabStop = false;
            this.btncancelar.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(102)))), ((int)(((byte)(132)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.txtdtValidade);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.txtProduto);
            this.panel4.Location = new System.Drawing.Point(0, 136);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(629, 105);
            this.panel4.TabIndex = 245;
            // 
            // txtdtValidade
            // 
            this.txtdtValidade.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.txtdtValidade.Location = new System.Drawing.Point(234, 56);
            this.txtdtValidade.Mask = "00/00/0000";
            this.txtdtValidade.Name = "txtdtValidade";
            this.txtdtValidade.Size = new System.Drawing.Size(313, 26);
            this.txtdtValidade.TabIndex = 249;
            this.txtdtValidade.ValidatingType = typeof(System.DateTime);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(29, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(199, 24);
            this.label4.TabIndex = 212;
            this.label4.Text = "Data do Validade:";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.txtatual);
            this.panel5.Controls.Add(this.txtsaida);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.nudatual);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.nudsaida);
            this.panel5.Location = new System.Drawing.Point(0, 228);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(629, 133);
            this.panel5.TabIndex = 246;
            // 
            // txtatual
            // 
            this.txtatual.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.txtatual.Location = new System.Drawing.Point(177, 78);
            this.txtatual.Mask = "00/00/0000";
            this.txtatual.Name = "txtatual";
            this.txtatual.Size = new System.Drawing.Size(173, 26);
            this.txtatual.TabIndex = 250;
            this.txtatual.ValidatingType = typeof(System.DateTime);
            // 
            // txtsaida
            // 
            this.txtsaida.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.txtsaida.Location = new System.Drawing.Point(177, 33);
            this.txtsaida.Mask = "00/00/0000";
            this.txtsaida.Name = "txtsaida";
            this.txtsaida.Size = new System.Drawing.Size(173, 26);
            this.txtsaida.TabIndex = 249;
            this.txtsaida.ValidatingType = typeof(System.DateTime);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(68, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 21);
            this.label2.TabIndex = 234;
            this.label2.Text = "Data Atual:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(399, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 21);
            this.label3.TabIndex = 232;
            this.label3.Text = "Qtd Atual:";
            // 
            // nudatual
            // 
            this.nudatual.BackColor = System.Drawing.Color.WhiteSmoke;
            this.nudatual.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudatual.Location = new System.Drawing.Point(501, 78);
            this.nudatual.Name = "nudatual";
            this.nudatual.Size = new System.Drawing.Size(46, 26);
            this.nudatual.TabIndex = 233;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(43, 36);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 21);
            this.label7.TabIndex = 234;
            this.label7.Text = "Data de Saída:";
            // 
            // nudEntrega
            // 
            this.nudEntrega.BackColor = System.Drawing.Color.WhiteSmoke;
            this.nudEntrega.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudEntrega.Location = new System.Drawing.Point(503, 99);
            this.nudEntrega.Name = "nudEntrega";
            this.nudEntrega.Size = new System.Drawing.Size(46, 26);
            this.nudEntrega.TabIndex = 221;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(382, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 21);
            this.label6.TabIndex = 219;
            this.label6.Text = "Qtd Entrega:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 21);
            this.label1.TabIndex = 226;
            this.label1.Text = "Data de Entrega:";
            // 
            // txtdtEntrega
            // 
            this.txtdtEntrega.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.txtdtEntrega.Location = new System.Drawing.Point(179, 97);
            this.txtdtEntrega.Mask = "00/00/0000";
            this.txtdtEntrega.Name = "txtdtEntrega";
            this.txtdtEntrega.Size = new System.Drawing.Size(173, 26);
            this.txtdtEntrega.TabIndex = 248;
            this.txtdtEntrega.ValidatingType = typeof(System.DateTime);
            // 
            // EditarEstoque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(620, 428);
            this.Controls.Add(this.txtdtEntrega);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btncancelar);
            this.Controls.Add(this.nudEntrega);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.btnalterar);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.btnsalvar);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EditarEstoque";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EditarEstoque";
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnalterar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnsalvar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btncancelar)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label ID_Festa;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtProduto;
        private System.Windows.Forms.DomainUpDown nudsaida;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.PictureBox btnalterar;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.PictureBox btnsalvar;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox btncancelar;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DomainUpDown nudatual;
        private System.Windows.Forms.ComboBox cbofornecedores;
        private System.Windows.Forms.MaskedTextBox txtdtValidade;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox txtatual;
        private System.Windows.Forms.MaskedTextBox txtsaida;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DomainUpDown nudEntrega;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox txtdtEntrega;
    }
}