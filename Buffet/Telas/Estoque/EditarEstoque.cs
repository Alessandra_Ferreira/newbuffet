﻿using Buffet.Db.Estoque;
using Buffet.Db.Fornecedores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class EditarEstoque : Form
    {
        public EditarEstoque()
        {
            InitializeComponent();
            LoadScreen();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Estoque atualizado com sucesso!", "Magic Buffet", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Hide();

        }

        void LoadScreen()
        {

            btnalterar.Visible = false;
            btnalterar.Enabled = false;

            btnsalvar.Visible = true;
            btnsalvar.Enabled = true;

            CarregarCombos();
        }
        void CarregarCombos()
        {
            Business_Fornecedores business = new Business_Fornecedores();
            List<DTO_Fornecedores> lista = business.Listar();

            cbofornecedores.ValueMember = nameof(DTO_Fornecedores.ID_Fornecedor);
            cbofornecedores.DisplayMember = nameof(DTO_Fornecedores.Nome_Fantasia);
            cbofornecedores.DataSource = lista;
        }
      
        private void button6_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

            try
            {
                DTO_Estoque dto = new DTO_Estoque();
                dto.Produto = txtProduto.Text;
                dto.Quantidade_Entrada = Convert.ToInt32(nudEntrega.Text);
                dto.Quantidade_Saida = Convert.ToInt32(nudsaida.Text);
                dto.Quantidae_Atual = Convert.ToInt32(nudatual.Text);
                dto.Data_Entrada = Convert.ToDateTime(txtdtEntrega.Text);
                dto.Data_Saida = Convert.ToDateTime(txtsaida.Text);
                dto.Data_Atual = DateTime.Now;
                dto.Data_Validade = Convert.ToDateTime(txtdtValidade.Text);


                Business_Estoque business_Estoque = new Business_Estoque();
                business_Estoque.Salvar(dto);     

                MessageBox.Show("Salvo com sucesso!", 
                                "Magic Buffet",
                                MessageBoxButtons.OK, 
                                MessageBoxIcon.Information);
                Hide();

            }
            catch (Exception)
            {
                MessageBox.Show("Tenha certeza de que preencheu corretamente a todos os campos!",
                                "Erro", 
                                MessageBoxButtons.AbortRetryIgnore, 
                                MessageBoxIcon.Error);
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            DTO_Estoque dto = new DTO_Estoque();
            dto.Produto = txtProduto.Text;
            dto.Quantidade_Entrada = Convert.ToInt32(nudEntrega.Text);
            dto.Quantidade_Saida = Convert.ToInt32(nudsaida.Text);
            dto.Quantidae_Atual = Convert.ToInt32(nudatual.Text);
            dto.Data_Entrada = Convert.ToDateTime(txtdtEntrega.Text);
            dto.Data_Saida = Convert.ToDateTime(txtsaida.Text);
            dto.Data_Atual = DateTime.Now;
            dto.Data_Validade = Convert.ToDateTime(txtdtValidade.Text);

            Business_Estoque business_Estoque = new Business_Estoque();
            business_Estoque.Alterar(dto);

            MessageBox.Show("Estoque atualizado com sucesso!",
                            "Magic Buffet", 
                            MessageBoxButtons.OK, 
                            MessageBoxIcon.Information);
            Hide();
        }

        public void LoadScreenAlterar(DTO_Estoque dto)
        {
            
            txtProduto.Text = dto.Produto ;
            nudEntrega.Text = dto.Quantidade_Entrada.ToString();
            nudsaida.Text = dto.Quantidade_Saida.ToString();
            nudatual.Text = dto.Quantidae_Atual.ToString();
            txtdtEntrega.Text = dto.Data_Entrada.ToString();
            txtsaida.Text = dto.Data_Saida.ToString();
            txtdtValidade.Text = dto.Data_Validade.ToString();

            btnalterar.Visible = true;
            btnalterar.Enabled = true;

            btnsalvar.Visible = false;
            btnsalvar.Enabled = false;


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void label25_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void label21_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
