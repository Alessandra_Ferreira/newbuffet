﻿using Buffet.Db.Clientes;
using Buffet.Utilitarios;
using Correios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class CadastroCliente : Form
    {
        public CadastroCliente()
        {
            InitializeComponent();
            LoadScreen();

            btnalterar.Enabled = false;
            btnalterar.Visible = false;
            lblalterar.Enabled = false;
            lblalterar.Visible = false;

            //Desabilete as labels também. Caso contrário, irá aparecer uma LABEL com o nome ALTERAR

            /*
                A Lógica é bastante simples. Basicamente, quem estiver programando terá que pegar o valor do controle
                No caso uma Label, e após isso, chamar a função Visible e desabilitar para false.

                OBS: Só tem que ter cuidado na hora de chamar o método de Alterar pois, nesse momento, essa mesma LABEL
                Deverá ficar vísivel. Para tal, apenas pegue o nome do controle e atruibua o método VISIBLE passsando o valor
                Para TRUE
            */
        }

        private void label21_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            //Desculpa ai Augusto...
            DTO_Clientes dto = new DTO_Clientes();

            //Mudando um pouco do método para não dar conflito
            SalvarNha(dto);

            OBUSINESSVEMAQUI.Salvar(dto);


            MessageBox.Show("Cliente cadastrado com sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
           
        }
        //Essa objeto deve ficar como uma variável não instânciada, 
        //pois caso contrário, ela não poderá servir para todos os métodos
        DTO_Clientes nha;

        Business_Cliente OBUSINESSVEMAQUI = new Business_Cliente();

        public void LoadScreenAlterar(DTO_Clientes nha)
        {
            //O objeto de escopo acima estará recebendo o valor de 
            this.nha = nha;

            
            txtnome.Text = nha.Nome;
            txtcpf.Text = nha.CPF;
            txtprimeiraOB.Text = nha.Observacao_Cadastro;
            dtpnascimento.Value = nha.Data_Nascimento;
            dtpCadastro.Value = nha.Data_Cadastro;

            //ENDEREÇO
            txtcep.Text = nha.CEP;
            cbouf.SelectedItem = nha.UF;
            txtcidade.Text = nha.Cidade;
            txtendendereco.Text = nha.Endereco;
            txtNumero.Text = nha.Numero;

            //Novamente a escrita do controle estava incorreta!
            txtSegundaOBS.Text = nha.Observacoes_Endereco;
            TxtComplemento.Text= nha.Complemento;
            txtbairro.Text = nha.Bairro;

            //CONTATOS
            txtTelefone.Text = nha.Telefone;
            txtCelular.Text = nha.Celular;
            txtEmail.Text = nha.Email;

            btnalterar.Enabled = true;
            btnalterar.Visible = true;
        }

        private void SalvarNha(DTO_Clientes dto)
        {
            
            dto.Nome = txtnome.Text;
            dto.CPF = txtcpf.Text;
            dto.Observacao_Cadastro = txtprimeiraOB.Text;
            dto.Data_Nascimento = dtpnascimento.Value;
            dto.Data_Cadastro = dtpCadastro.Value;

            //ENDEREÇO
            dto.CEP = txtcep.Text;
            dto.UF = cbouf.SelectedItem.ToString();
            dto.Cidade = txtcidade.Text;
            dto.Endereco = txtendendereco.Text;
            dto.Numero = txtNumero.Text;

            //Esta parte estava uma cópia da NHA acima.
            dto.Observacoes_Endereco = txtSegundaOBS.Text;
            dto.Complemento = TxtComplemento.Text;
            dto.Bairro = txtbairro.Text;

            //CONTATOS
            dto.Telefone = txtTelefone.Text;
            dto.Celular = txtCelular.Text;
            dto.Email = txtEmail.Text;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            //Mudando um pouco do método para não dar conflito
            //Pois como o método de ALTERAR precisa das informações, eu apenas chama a variável de escopo
            SalvarNha(nha);
            OBUSINESSVEMAQUI.Alterar(nha);
            

            MessageBox.Show("Cliente alterado com sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Hide();

        }

        private void LoadScreen()
        {
            UF uf = new UF();
            cbouf.DataSource = uf.UFS();
        }

        private void btncep_Click(object sender, EventArgs e)
        {
            ProcurarCEP();
        }
        //private void ProcurarCEP()
        //{
        //    string cep = txtCEP.Text;

        //    CEPbusca ceepbusca = new CEPbusca();
        //    ceepbusca.BuscarCep(cep);

        //    Dto_cep ctocep = new Dto_cep();
        //    cboUF.SelectedItem = ctocep.uf;
        //    txtCIdade.Text = ctocep.Cidade;
        //    txtEndereco.Text = ctocep.End;
        //    txtBairro.Text = ctocep.Bairro;
        //}
        private void ProcurarCEP()
        {
            string cep = txtcep.Text;
            var service = new CorreiosApi();
            var dados = service.consultaCEP(cep);
            txtcidade.Text = dados.cidade;
            txtendendereco.Text = dados.end;
            txtbairro.Text = dados.bairro;
            cbouf.SelectedItem = dados.uf;




        }

        private void txtCIdade_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

