﻿using Buffet.Db.Pedidos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class ConsultarReservas : Form
    {
        public ConsultarReservas()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Telas.REalizarPedido tela = new Telas.REalizarPedido();
            tela.Show();
            Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Hide();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void label21_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ConsultarReservas_Load(object sender, EventArgs e)
        {

        }

        private void dgvPedidofesta_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            carregarview();
        }

        private void carregarview()
        {
            
            Business_Pedidos business = new Business_Pedidos();
            dgvPedidofesta.AutoGenerateColumns = false;
            dgvPedidofesta.DataSource = business.consultarPedido(txtnome.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            carregarview();
        }
    }
}
