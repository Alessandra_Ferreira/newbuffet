﻿using Buffet.Db.Funcionarios;
using Buffet.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas.Usuario
{
    public partial class Senha : Form
    {
        public Senha()
        {
            InitializeComponent();
        }
        CriacaodeCodigo cod;
        string codigo;

        private void EnviarConfirmacao()
        {

            cod = new CriacaodeCodigo();
             codigo = cod.Codigo4();

            string para = txtemail.Text;
            string copiaoculta = "smarti7empresa@gmail.com";
            string copia = "smarti7empresa@gmail.com";
            string assunto = "Modificação de senha";
            bool comhtml = false;
            string mensagem = 
                "Olá, informamos que o usuario" 
                + txtnomeusuario.Text
                +" está realizando o pedido para efetuar a " 
                +"troca de senha, caso não seja você, ignore esta mensagem"
                +"caso seja envie o codigo abaixo "
                + codigo;

            Email email = new Email();
            email.EnviarGmail(para, copia, copiaoculta, assunto, mensagem, comhtml);
            MessageBox.Show("Teste");
        }

        private bool ConferirCOdigo()
        {
            bool codcorreto;
            string codinformado = txtcod.Text;
            if (codigo == codinformado)
            {
                MessageBox.Show("Troca de senha realizada com sucesso",
                                "Magic Buffet",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                codcorreto = true;
                
            }
            else
            {
               DialogResult re = MessageBox.Show("Codigo incorreto, deseja enviar um novo código ?",
                                  "Magic Buffet",
                                  MessageBoxButtons.YesNoCancel,
                                  MessageBoxIcon.Information);
                if (re == DialogResult.Yes)
                {
                    EnviarConfirmacao();
                }
                if (re == DialogResult.Cancel)
                {
                    LoginUsuarioComum tela = new LoginUsuarioComum();
                    tela.Show();
                    this.Close();
                }
               
                codcorreto = false;
            }
            return codcorreto;
        }

        private void btnenviaremail_Click(object sender, EventArgs e)
        {
            EnviarConfirmacao();
        }

        private void btncod_Click(object sender, EventArgs e)
        {
           bool cod = ConferirCOdigo();

            if (cod == true)
            {
                //Business_Funcionario bus = new Business_Funcionario();

                //DTO_Fucionario dto = new DTO_Fucionario();
                //dto.Nome = txtnomeusuario.Text;
                //bus.Consultar(dto.Nome);
                //bus.Alterar(dto);

                MessageBox.Show("Senha alterada com sucesso",
                                "Magic Buffet",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);

            }
        }
    }
}
