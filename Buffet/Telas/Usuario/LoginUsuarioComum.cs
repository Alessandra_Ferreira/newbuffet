﻿using Buffet.Db.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class LoginUsuarioComum : Form
    {
        public LoginUsuarioComum()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Você logou com sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Hide();


        }

        private void label21_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Business_Funcionario business = new Business_Funcionario();
            DTO_Fucionario dto = business.Logar(txtusuario.Text, txtsenha.Text);

            if (dto != null)
            {
                UserSession.UsuarioLogado = dto;
                Form1 menu = new Form1();
                menu.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Dados inválidos",
                                "Erro",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Telas.Usuario.Senha senha = new Usuario.Senha();
            senha.Show();

        }
    }
}
