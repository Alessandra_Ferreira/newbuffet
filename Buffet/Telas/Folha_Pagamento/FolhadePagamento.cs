﻿using Buffet.Db.FolhaDePagamento;
using Buffet.Db.Funcionarios;
using Buffet.Utilitarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class FolhadePagamento : Form
    {
        public FolhadePagamento()
        {
            InitializeComponent();
            LoadScreen();

        }

        private void LoadScreen() {

            //Chamando os funcionarios
            Business_Funcionario db = new Business_Funcionario();
            List<DTO_Fucionario> lista = db.Listar();

            //Mostrando para o combo box o que ele precisa mostrar
            cbofuncionario.ValueMember = nameof(DTO_Fucionario.ID_Funcionario);
            cbofuncionario.DisplayMember = nameof(DTO_Fucionario.Nome);

            /*Aqui o combo box consegue "sugerir" algo conforme
             * o usuario digita no combo e o programa mostra sugestões
             * para ele.
             * A primeira linha faz com que ocorra a sugestão de algo
             * A segunda mostra o formato que se deseja que a sugestão ocorra
             */
            cbofuncionario.AutoCompleteMode = AutoCompleteMode.Suggest;
            cbofuncionario.AutoCompleteSource = AutoCompleteSource.ListItems;
            cbofuncionario.DataSource = lista;
                }
        double Salario = 0;
        
       

        private void Calculos()
        {
            string dias = txtDias.Text;
            string diasUteis = txtDiasUteis.Text;
            string SalarioBruto = txtSalarioBruto.Text;
            string HorasTrabalho = UDHorasTrabalho.Text;
            string DiasTrabalho = UDDiasTrabalho.Text;
            string Horast50 = nudHrEx50.Text;
            string horast100 = nudHrEx100.Text;
            int Horas50 = Convert.ToInt32(Horast50);
            int Horas100 = Convert.ToInt32(horast100);


            int dias1 = Convert.ToInt32(dias);
            int DiasTRabmes = Convert.ToInt32(DiasTrabalho);
            double SalarioBruto1 = Convert.ToDouble(SalarioBruto);
            int HorasT = Convert.ToInt32(HorasTrabalho);
            int diasT = Convert.ToInt32(DiasTrabalho);

            DTO_Fucionario funcionario = cbofuncionario.SelectedItem as DTO_Fucionario;

            int id = funcionario.ID_Funcionario;
            CalcularFolhadePagamento Calculos = new CalcularFolhadePagamento();
            double Salario = Calculos.Calculo(SalarioBruto1, dias1, DiasTRabmes, Horas50, Horas100,id);
            lblLiquido.Text = Salario.ToString();

            
            
        }
        
        private void label9_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        

        private void btnPDF_Click(object sender, EventArgs e)
        {
            Relatorio();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {

        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            Calculos();
        }

        private void txtusuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Form1 oi = new Form1();
            oi.Show();

        }


        //Relatorio provisorio, em breve será trocado;
        private void Relatorio()
        {

            string
                  cargo,
                  nome,
                  diastrabalhados,
                  valealimentacao = "",
                 
                  conteudo;

            nome = cbofuncionario.SelectedItem.ToString();
            cargo = cboCargos.Text;

            diastrabalhados = UDDiasTrabalho.Text;



            conteudo = "O funcionario "
                        + nome
                        + " que efetuou o trabalho de "
                        + cargo
                        + " trabalhou durante "
                        + diastrabalhados
                        
                        
                        + " recebendo "
                        + Salario
                        + "Esse valor possui os descontos de : "

                        + " Vale Alimentação : "
                        + valealimentacao;


            PDF pdf = new PDF();
            pdf.pdf(conteudo);


        }

        private void FolhadePagamento_Load(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }



    }
}
