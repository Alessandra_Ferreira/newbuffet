﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet
{
    public partial class TelaSplash : Form
    {
        public TelaSplash()
        {
            InitializeComponent();

            //Começa a contagem para o início do SPLASH
            Task.Factory.StartNew(() =>
            {
                //Espera cerca de 5 segundos.
                System.Threading.Thread.Sleep(10000);

                Invoke(new Action(() =>
                {
                    Telas.LoginUsuarioComum TELA = new Telas.LoginUsuarioComum();
                    TELA.Show();
                    this.Hide();

                }));
            });
        }

        private void TelaSplash_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click_2(object sender, EventArgs e)
        {

        }
    }
}
