﻿using Buffet.Db.Agenda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas.Agenda
{
    public partial class Agenda : Form
    {
        public Agenda()
        {
            InitializeComponent();
            CarregarCombo();
        }

        private void CarregarCombo ()
        {
            List<string> dias = new List<string>();
            dias.Add("Selecionar");
            dias.Add("Domingo");
            dias.Add("Segunda-Feira");
            dias.Add("Terça-Feira");
            dias.Add("Quarta-Feira");
            dias.Add("Quinta-Feira");
            dias.Add("Sexta-Feira");
            dias.Add("Sábado");

            cboDiaSemana.DataSource = dias;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            DTO_agenda dto = new DTO_agenda();
            dto.hr_horario = txthora.ToString();
            dto.dt_diasemana = cboDiaSemana.SelectedItem.ToString();
            dto.obs_observacao = txtobs.Text;
            dto.dt_dataatual = DateTime.Now;
            dto.dt_datareserva = Convert.ToDateTime(monthCalendar1.SelectionStart);

            Business_agenda business = new Business_agenda();
            business.Salvar(dto);
        }
    }
}
