﻿using Buffet.Db.Fornecedores;
using Buffet.Db.Funcionarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class ConsultarFuncionarios : Form
    {
        public ConsultarFuncionarios()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Database_Funcionario db = new Database_Funcionario();
            if (e.ColumnIndex == 5)
            {
                DTO_Fucionario dto = dvgCon.CurrentRow.DataBoundItem as DTO_Fucionario;
                int id = dto.ID_Funcionario;
                db.Remover(id);
            }
            if (e.ColumnIndex == 6)
            {
                DTO_Fucionario dto = dvgCon.CurrentRow.DataBoundItem as DTO_Fucionario;
                CadastroFuncionarios tela = new CadastroFuncionarios();

                DTO_Fucionario alt = new DTO_Fucionario();
                tela.LoadScreenAlterar(alt);
                tela.Show();
                this.Hide();


            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            Hide();


        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void CarreGrid()
        {
            Business_Funcionario business = new Business_Funcionario();
            dvgCon.AutoGenerateColumns = false;
            dvgCon.DataSource = business.Consultar(txtnome.Text);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            CarreGrid();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            CarreGrid();


        }
    }
}
