﻿namespace Buffet.Telas
{
    partial class CadastroFuncionarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbocargo = new System.Windows.Forms.ComboBox();
            this.txtbairro = new System.Windows.Forms.TextBox();
            this.txtcidade = new System.Windows.Forms.TextBox();
            this.txtcomplemento = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.dtpcadastro = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.txtendendereco = new System.Windows.Forms.TextBox();
            this.dtpdatasaida = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtnumero = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cbouf = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtcelular = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txttelefone = new System.Windows.Forms.MaskedTextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label28 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtobservacaofuncionarios = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtobservacoesendereco = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btncep = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.cbosituacao = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.rdnadm = new System.Windows.Forms.RadioButton();
            this.rdnsempermissao = new System.Windows.Forms.RadioButton();
            this.txtusuario = new System.Windows.Forms.TextBox();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtcpf = new System.Windows.Forms.MaskedTextBox();
            this.txtcep = new System.Windows.Forms.MaskedTextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.rbttercerizados = new System.Windows.Forms.RadioButton();
            this.rbtestoque = new System.Windows.Forms.RadioButton();
            this.rbtfornecedor = new System.Windows.Forms.RadioButton();
            this.rbtfuncionario = new System.Windows.Forms.RadioButton();
            this.rbtpedido = new System.Windows.Forms.RadioButton();
            this.rbtcliente = new System.Windows.Forms.RadioButton();
            this.label30 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.SuspendLayout();
            // 
            // cbocargo
            // 
            this.cbocargo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbocargo.Font = new System.Drawing.Font("Candara", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbocargo.FormattingEnabled = true;
            this.cbocargo.Items.AddRange(new object[] {
            "Cozinheiro",
            "Faxineiro",
            "Organizador de Eventos"});
            this.cbocargo.Location = new System.Drawing.Point(109, 69);
            this.cbocargo.Name = "cbocargo";
            this.cbocargo.Size = new System.Drawing.Size(200, 21);
            this.cbocargo.TabIndex = 7;
            // 
            // txtbairro
            // 
            this.txtbairro.Location = new System.Drawing.Point(125, 251);
            this.txtbairro.Name = "txtbairro";
            this.txtbairro.Size = new System.Drawing.Size(323, 20);
            this.txtbairro.TabIndex = 19;
            // 
            // txtcidade
            // 
            this.txtcidade.Location = new System.Drawing.Point(284, 166);
            this.txtcidade.Name = "txtcidade";
            this.txtcidade.Size = new System.Drawing.Size(164, 20);
            this.txtcidade.TabIndex = 16;
            // 
            // txtcomplemento
            // 
            this.txtcomplemento.Location = new System.Drawing.Point(7, 251);
            this.txtcomplemento.Name = "txtcomplemento";
            this.txtcomplemento.Size = new System.Drawing.Size(109, 20);
            this.txtcomplemento.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(106, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 17);
            this.label10.TabIndex = 49;
            this.label10.Text = "Cargo";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(4, 231);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 17);
            this.label8.TabIndex = 47;
            this.label8.Text = "Complemento";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(350, 189);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 17);
            this.label4.TabIndex = 46;
            this.label4.Text = "N°";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(315, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 17);
            this.label2.TabIndex = 44;
            this.label2.Text = "Data Admissão";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(7, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 17);
            this.label6.TabIndex = 43;
            this.label6.Text = "CPF";
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(48, 29);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(261, 20);
            this.txtnome.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 37;
            this.label1.Text = "Funcionário";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(7, 28);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(35, 20);
            this.txtID.TabIndex = 1;
            // 
            // dtpcadastro
            // 
            this.dtpcadastro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dtpcadastro.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpcadastro.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dtpcadastro.Location = new System.Drawing.Point(315, 28);
            this.dtpcadastro.Name = "dtpcadastro";
            this.dtpcadastro.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dtpcadastro.RightToLeftLayout = true;
            this.dtpcadastro.Size = new System.Drawing.Size(134, 22);
            this.dtpcadastro.TabIndex = 4;
            this.dtpcadastro.Value = new System.DateTime(2018, 9, 11, 0, 0, 0, 0);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(312, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 17);
            this.label7.TabIndex = 57;
            this.label7.Text = "Data Saída";
            // 
            // txtendendereco
            // 
            this.txtendendereco.Location = new System.Drawing.Point(7, 208);
            this.txtendendereco.Name = "txtendendereco";
            this.txtendendereco.Size = new System.Drawing.Size(340, 20);
            this.txtendendereco.TabIndex = 17;
            // 
            // dtpdatasaida
            // 
            this.dtpdatasaida.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpdatasaida.Location = new System.Drawing.Point(315, 68);
            this.dtpdatasaida.Name = "dtpdatasaida";
            this.dtpdatasaida.Size = new System.Drawing.Size(134, 22);
            this.dtpdatasaida.TabIndex = 8;
            this.dtpdatasaida.Value = new System.DateTime(2018, 9, 25, 0, 0, 0, 0);
            this.dtpdatasaida.ValueChanged += new System.EventHandler(this.dtpdatasaida_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(4, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 17);
            this.label3.TabIndex = 61;
            this.label3.Text = "CEP";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(645, 22);
            this.panel2.TabIndex = 63;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(591, -4);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(24, 27);
            this.label21.TabIndex = 253;
            this.label21.Text = "_";
            this.label21.Click += new System.EventHandler(this.label21_Click_1);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(621, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(22, 23);
            this.label25.TabIndex = 252;
            this.label25.Text = "X";
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold);
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(5, 5);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(169, 16);
            this.label20.TabIndex = 49;
            this.label20.Text = "Cadastro de Funcionário";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 49;
            this.label5.Text = "Endereço";
            // 
            // txtnumero
            // 
            this.txtnumero.Location = new System.Drawing.Point(353, 208);
            this.txtnumero.Name = "txtnumero";
            this.txtnumero.Size = new System.Drawing.Size(95, 20);
            this.txtnumero.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(122, 231);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 17);
            this.label11.TabIndex = 67;
            this.label11.Text = "Bairro";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(134, 168);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(22, 17);
            this.label12.TabIndex = 68;
            this.label12.Text = "UF";
            // 
            // cbouf
            // 
            this.cbouf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbouf.FormattingEnabled = true;
            this.cbouf.Items.AddRange(new object[] {
            "SP",
            "RJ"});
            this.cbouf.Location = new System.Drawing.Point(162, 164);
            this.cbouf.Name = "cbouf";
            this.cbouf.Size = new System.Drawing.Size(45, 21);
            this.cbouf.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(221, 168);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 17);
            this.label13.TabIndex = 70;
            this.label13.Text = "Cidade";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.txtcelular);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Controls.Add(this.txttelefone);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.pictureBox3);
            this.panel4.Controls.Add(this.label28);
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Controls.Add(this.pictureBox2);
            this.panel4.Location = new System.Drawing.Point(-3, 313);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(643, 112);
            this.panel4.TabIndex = 65;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // txtcelular
            // 
            this.txtcelular.Location = new System.Drawing.Point(227, 28);
            this.txtcelular.Mask = " 0000-0000";
            this.txtcelular.Name = "txtcelular";
            this.txtcelular.Size = new System.Drawing.Size(200, 20);
            this.txtcelular.TabIndex = 50;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(584, 71);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(49, 17);
            this.label26.TabIndex = 215;
            this.label26.Text = "Alterar";
            // 
            // txttelefone
            // 
            this.txttelefone.Location = new System.Drawing.Point(6, 28);
            this.txttelefone.Mask = " 0000-0000";
            this.txttelefone.Name = "txttelefone";
            this.txttelefone.Size = new System.Drawing.Size(201, 20);
            this.txttelefone.TabIndex = 49;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(515, 71);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(63, 17);
            this.label27.TabIndex = 214;
            this.label27.Text = "Cancelar";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Buffet.Properties.Resources.alterar;
            this.pictureBox3.Location = new System.Drawing.Point(580, 35);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(58, 33);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 212;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(463, 71);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(46, 17);
            this.label28.TabIndex = 213;
            this.label28.Text = "Salvar";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Buffet.Properties.Resources._1024px_Deletion_icon_svg;
            this.pictureBox1.Location = new System.Drawing.Point(518, 35);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(58, 33);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 210;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Buffet.Properties.Resources._678134_sign_check_512;
            this.pictureBox2.Location = new System.Drawing.Point(456, 35);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(58, 33);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 211;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(5, 138);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 19);
            this.label14.TabIndex = 48;
            this.label14.Text = "Contatos";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 323);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 17);
            this.label15.TabIndex = 71;
            this.label15.Text = "Telefone 1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(7, 93);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 17);
            this.label16.TabIndex = 73;
            this.label16.Text = "Usuário";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(2, 366);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 17);
            this.label17.TabIndex = 75;
            this.label17.Text = "Email";
            // 
            // txtemail
            // 
            this.txtemail.Location = new System.Drawing.Point(5, 386);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(421, 20);
            this.txtemail.TabIndex = 24;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(454, 49);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 17);
            this.label18.TabIndex = 80;
            this.label18.Text = "Observações";
            // 
            // txtobservacaofuncionarios
            // 
            this.txtobservacaofuncionarios.Location = new System.Drawing.Point(455, 69);
            this.txtobservacaofuncionarios.Multiline = true;
            this.txtobservacaofuncionarios.Name = "txtobservacaofuncionarios";
            this.txtobservacaofuncionarios.Size = new System.Drawing.Size(173, 20);
            this.txtobservacaofuncionarios.TabIndex = 9;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(455, 166);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(92, 17);
            this.label19.TabIndex = 82;
            this.label19.Text = "Observações";
            // 
            // txtobservacoesendereco
            // 
            this.txtobservacoesendereco.Location = new System.Drawing.Point(458, 186);
            this.txtobservacoesendereco.Multiline = true;
            this.txtobservacoesendereco.Name = "txtobservacoesendereco";
            this.txtobservacoesendereco.Size = new System.Drawing.Size(173, 85);
            this.txtobservacoesendereco.TabIndex = 21;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.btncep);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Location = new System.Drawing.Point(-2, 156);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(643, 164);
            this.panel3.TabIndex = 83;
            // 
            // btncep
            // 
            this.btncep.BackgroundImage = global::Buffet.Properties.Resources.search;
            this.btncep.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btncep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncep.Location = new System.Drawing.Point(111, 10);
            this.btncep.Name = "btncep";
            this.btncep.Size = new System.Drawing.Size(17, 18);
            this.btncep.TabIndex = 49;
            this.btncep.UseVisualStyleBackColor = true;
            this.btncep.Click += new System.EventHandler(this.btncep_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 137);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 19);
            this.label9.TabIndex = 48;
            this.label9.Text = "Endereço";
            // 
            // cbosituacao
            // 
            this.cbosituacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbosituacao.FormattingEnabled = true;
            this.cbosituacao.Items.AddRange(new object[] {
            "Ativo",
            "Inativo"});
            this.cbosituacao.Location = new System.Drawing.Point(455, 27);
            this.cbosituacao.Name = "cbosituacao";
            this.cbosituacao.Size = new System.Drawing.Size(173, 21);
            this.cbosituacao.TabIndex = 5;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(456, 9);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 17);
            this.label22.TabIndex = 84;
            this.label22.Text = "Situação";
            // 
            // rdnadm
            // 
            this.rdnadm.AutoSize = true;
            this.rdnadm.Checked = true;
            this.rdnadm.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnadm.Location = new System.Drawing.Point(189, 43);
            this.rdnadm.Name = "rdnadm";
            this.rdnadm.Size = new System.Drawing.Size(67, 26);
            this.rdnadm.TabIndex = 12;
            this.rdnadm.TabStop = true;
            this.rdnadm.Text = "ADM";
            this.rdnadm.UseVisualStyleBackColor = true;
            // 
            // rdnsempermissao
            // 
            this.rdnsempermissao.AutoSize = true;
            this.rdnsempermissao.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnsempermissao.Location = new System.Drawing.Point(275, 44);
            this.rdnsempermissao.Name = "rdnsempermissao";
            this.rdnsempermissao.Size = new System.Drawing.Size(153, 26);
            this.rdnsempermissao.TabIndex = 13;
            this.rdnsempermissao.Text = "Sem Permissão";
            this.rdnsempermissao.UseVisualStyleBackColor = true;
            // 
            // txtusuario
            // 
            this.txtusuario.Location = new System.Drawing.Point(7, 114);
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(164, 20);
            this.txtusuario.TabIndex = 10;
            // 
            // txtsenha
            // 
            this.txtsenha.Location = new System.Drawing.Point(175, 114);
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(164, 20);
            this.txtsenha.TabIndex = 11;
            this.txtsenha.UseSystemPasswordChar = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(223, 321);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 17);
            this.label23.TabIndex = 90;
            this.label23.Text = "Celular";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(175, 93);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(47, 17);
            this.label24.TabIndex = 91;
            this.label24.Text = "Senha";
            // 
            // txtcpf
            // 
            this.txtcpf.Location = new System.Drawing.Point(7, 69);
            this.txtcpf.Mask = "000.000.000-00";
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(96, 20);
            this.txtcpf.TabIndex = 6;
            this.txtcpf.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox1_MaskInputRejected);
            // 
            // txtcep
            // 
            this.txtcep.Location = new System.Drawing.Point(44, 166);
            this.txtcep.Name = "txtcep";
            this.txtcep.Size = new System.Drawing.Size(64, 20);
            this.txtcep.TabIndex = 14;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 26);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(642, 452);
            this.tabControl1.TabIndex = 254;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cbosituacao);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.txtcep);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.txtemail);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.txtnome);
            this.tabPage1.Controls.Add(this.txtobservacoesendereco);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtcpf);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.cbouf);
            this.tabPage1.Controls.Add(this.cbocargo);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.txtsenha);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.txtID);
            this.tabPage1.Controls.Add(this.txtnumero);
            this.tabPage1.Controls.Add(this.txtusuario);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.dtpcadastro);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txtendendereco);
            this.tabPage1.Controls.Add(this.txtbairro);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.txtcidade);
            this.tabPage1.Controls.Add(this.txtobservacaofuncionarios);
            this.tabPage1.Controls.Add(this.txtcomplemento);
            this.tabPage1.Controls.Add(this.dtpdatasaida);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(634, 426);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dados";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pictureBox7);
            this.tabPage2.Controls.Add(this.rbttercerizados);
            this.tabPage2.Controls.Add(this.rbtestoque);
            this.tabPage2.Controls.Add(this.rbtfornecedor);
            this.tabPage2.Controls.Add(this.rbtfuncionario);
            this.tabPage2.Controls.Add(this.rbtpedido);
            this.tabPage2.Controls.Add(this.rbtcliente);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.rdnadm);
            this.tabPage2.Controls.Add(this.rdnsempermissao);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(634, 426);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Permissões";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Buffet.Properties.Resources.locking;
            this.pictureBox7.Location = new System.Drawing.Point(241, 88);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(341, 291);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 106;
            this.pictureBox7.TabStop = false;
            // 
            // rbttercerizados
            // 
            this.rbttercerizados.AutoSize = true;
            this.rbttercerizados.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbttercerizados.Location = new System.Drawing.Point(25, 297);
            this.rbttercerizados.Name = "rbttercerizados";
            this.rbttercerizados.Size = new System.Drawing.Size(132, 26);
            this.rbttercerizados.TabIndex = 102;
            this.rbttercerizados.Text = "Tercerizados";
            this.rbttercerizados.UseVisualStyleBackColor = true;
            // 
            // rbtestoque
            // 
            this.rbtestoque.AutoSize = true;
            this.rbtestoque.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtestoque.Location = new System.Drawing.Point(25, 256);
            this.rbtestoque.Name = "rbtestoque";
            this.rbtestoque.Size = new System.Drawing.Size(94, 26);
            this.rbtestoque.TabIndex = 101;
            this.rbtestoque.Text = "Estoque";
            this.rbtestoque.UseVisualStyleBackColor = true;
            // 
            // rbtfornecedor
            // 
            this.rbtfornecedor.AutoSize = true;
            this.rbtfornecedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtfornecedor.Location = new System.Drawing.Point(25, 215);
            this.rbtfornecedor.Name = "rbtfornecedor";
            this.rbtfornecedor.Size = new System.Drawing.Size(120, 26);
            this.rbtfornecedor.TabIndex = 100;
            this.rbtfornecedor.Text = "Fornecedor";
            this.rbtfornecedor.UseVisualStyleBackColor = true;
            // 
            // rbtfuncionario
            // 
            this.rbtfuncionario.AutoSize = true;
            this.rbtfuncionario.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtfuncionario.Location = new System.Drawing.Point(25, 174);
            this.rbtfuncionario.Name = "rbtfuncionario";
            this.rbtfuncionario.Size = new System.Drawing.Size(122, 26);
            this.rbtfuncionario.TabIndex = 99;
            this.rbtfuncionario.Text = "Funcionario";
            this.rbtfuncionario.UseVisualStyleBackColor = true;
            // 
            // rbtpedido
            // 
            this.rbtpedido.AutoSize = true;
            this.rbtpedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtpedido.Location = new System.Drawing.Point(25, 133);
            this.rbtpedido.Name = "rbtpedido";
            this.rbtpedido.Size = new System.Drawing.Size(84, 26);
            this.rbtpedido.TabIndex = 98;
            this.rbtpedido.Text = "Pedido";
            this.rbtpedido.UseVisualStyleBackColor = true;
            // 
            // rbtcliente
            // 
            this.rbtcliente.AutoSize = true;
            this.rbtcliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtcliente.Location = new System.Drawing.Point(25, 92);
            this.rbtcliente.Name = "rbtcliente";
            this.rbtcliente.Size = new System.Drawing.Size(84, 26);
            this.rbtcliente.TabIndex = 97;
            this.rbtcliente.Text = "Cliente";
            this.rbtcliente.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Century Gothic", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(6, 37);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(157, 32);
            this.label30.TabIndex = 96;
            this.label30.Text = "Permissões";
            // 
            // CadastroFuncionarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(645, 479);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CadastroFuncionarios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CadastroFuncionarios";
            this.Load += new System.EventHandler(this.CadastroFuncionarios_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox cbocargo;
        private System.Windows.Forms.TextBox txtbairro;
        private System.Windows.Forms.TextBox txtcidade;
        private System.Windows.Forms.TextBox txtcomplemento;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.DateTimePicker dtpcadastro;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtendendereco;
        private System.Windows.Forms.DateTimePicker dtpdatasaida;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtnumero;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbouf;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtobservacaofuncionarios;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtobservacoesendereco;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbosituacao;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.RadioButton rdnadm;
        private System.Windows.Forms.RadioButton rdnsempermissao;
        private System.Windows.Forms.TextBox txtusuario;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.MaskedTextBox txtcpf;
        private System.Windows.Forms.MaskedTextBox txtcep;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.MaskedTextBox txtcelular;
        private System.Windows.Forms.MaskedTextBox txttelefone;
        private System.Windows.Forms.Button btncep;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.RadioButton rbttercerizados;
        private System.Windows.Forms.RadioButton rbtestoque;
        private System.Windows.Forms.RadioButton rbtfornecedor;
        private System.Windows.Forms.RadioButton rbtfuncionario;
        private System.Windows.Forms.RadioButton rbtpedido;
        private System.Windows.Forms.RadioButton rbtcliente;
        private System.Windows.Forms.Label label30;
    }
}