﻿using Buffet.Db.Terceirizados;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class CadastroTerceirizados : Form
    {
        public CadastroTerceirizados()
        {
            InitializeComponent();
            btnalterar.Visible = false;
            btnalterar.Enabled = false;
            lblalterar.Visible = false;
            dtpdatasaida.Enabled = false;
            dtpdatasaida.Visible = false;
            label7.Enabled = false;
            label7.Visible = false;
        }

        private void label21_Click(object sender, EventArgs e)
        {
            Close();

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            //Nesse caso, como não precisamos da Chave Primária, não precisamos utilizar a variável de objeto de esopo, e sim, instanciar a nossa
            DTO_Terceirizados dto = new DTO_Terceirizados();

            //Passando o valor do DTO para o método
            SalvarValores(dto);

            //Esquencendo de chamar o método da BUSINESS
            Business_Terceirizados db = new Business_Terceirizados();
            db.Salvar(dto);

            MessageBox.Show("Cadastro efetuado com sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK, 
                            MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Business_Terceirizados bus = new Business_Terceirizados();
            SalvarValores(dto);
            bus.Alterar(dto);

            MessageBox.Show("Alterado com sucesso!", 
                            "Magic Buffet", 
                            MessageBoxButtons.OK, 
                            MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Deseja cancelar o cadastro",
                            "Magic Buffet",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Information);
            Hide();
        }

        //Novamente... Para o método de ALTERAR funcionar, deve-se colocar este DTO como escolo não declarado
        //Ou seja, sem nenhum valor, para que assim, o mesmo consiga pegar o valor que irá ser passada da GRIEDVIEW
        //Caso contrário, quando você instância um objeto, ele já pega os valores contidos no própria objeto
        DTO_Terceirizados dto;

        private void SalvarValores(DTO_Terceirizados dto)
        {         
            dto.celular = txtcelular.Text;
            dto.CPF_CNPJ = txtcpfcnpj.Text;
            dto.email = txtemail.Text;
            dto.nome_Fantasia = txtfantasiaapelido.Text;
            dto.observacoes = txtobservacoes.Text;
            dto.telefone = txttelefone.Text;
            dto.Situacao = cbostituacao.SelectedItem.ToString();
            dto.Data_Cadastro = dtpdatacadastro.Value;
            //dto.Data_Saida =  dtpdatasaida.;
        }

        public void LoadScreenALterar(DTO_Terceirizados dto)
        {
            //Objeto de escopo. Se você passar o cursor do mouse por cima, irá perceber que o mesmo pega o valor da
            //Variável criada lá à acima e não a que está de paramêtro.
            //Por que isso?
            //Porque o dto que estará vindo de paramêtro é o que vem da Gried. Contendo todos os valores de um determinado registro (Linha)
            //Com isso, declaramos o nosso objeto de escopo, e o mesmo irá receber os valores que vinheram como paramêtro do MÉTODO LOADSCREEN
            //Pois dessa forma, iremos ter o valor do ID que será utilizado para fazer a alteração dos dados. Uffa!
            this.dto = dto;

            txtcelular.Text = dto.celular;
            txtcpfcnpj.Text = dto.CPF_CNPJ;
            txtemail.Text = dto.email;
            txtfantasiaapelido.Text = dto.nome_Fantasia;
            txtobservacoes.Text = dto.observacoes;
            txttelefone.Text = dto.telefone;
            cbostituacao.SelectedItem = dto.Situacao;
            dtpdatacadastro.Value = dto.Data_Cadastro;
            //dtpdatasaida.Value = dto.Data_Saida;

            btnsalvar.Visible = false;
            btnsalvar.Enabled = false;
            lblsalvar.Visible = false;

            btnalterar.Visible = true;
            btnalterar.Enabled = true;
            lblalterar.Visible = true;

            dtpdatasaida.Enabled = true;
            dtpdatasaida.Visible = true;
                label7.Enabled =   true;
                label7.Visible =   true;

        }

        private void CadastroTerceirizados_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
            Hide();
        }
    }
}
