﻿using Buffet.Db.Terceirizados;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class ConsultarTerceirizados : Form
    {
        public ConsultarTerceirizados()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();

        }

        private void label21_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
          Business_Terceirizados business = new Business_Terceirizados();
            
            if (e.ColumnIndex == 4)
            {
                DTO_Terceirizados dto = dataGridView1.CurrentRow.DataBoundItem as DTO_Terceirizados;

                CadastroTerceirizados tela = new CadastroTerceirizados();
                tela.LoadScreenALterar(dto);
                tela.Show();

                this.Hide();
            }
            if (e.ColumnIndex == 5)
            {
                DTO_Terceirizados dto = dataGridView1.CurrentRow.DataBoundItem as DTO_Terceirizados;
                business.Remover(dto);

                CarregarGried();
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            CarregarGried();
        }

        private void CarregarGried()
        {
            //Vamos deixar esse código mais CORINTHIANS?
            DTO_Terceirizados dto = new DTO_Terceirizados();
            dto.nome_Fantasia = txtpesquisa.Text;

            Business_Terceirizados db = new Business_Terceirizados();
            List<DTO_Terceirizados> consult = db.Consultar(dto);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = consult;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CarregarGried();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            CarregarGried();
        }
    }
}
