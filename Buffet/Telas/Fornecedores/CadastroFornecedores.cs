﻿using Buffet.Db.Fornecedores;
using Buffet.Db.Funcionarios;
using Buffet.Utilitarios;
using Correios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class CadastroFornecedores : Form
    {
        public CadastroFornecedores()
        {
            InitializeComponent();
            LoadScreen();
    
        }

        private void LoadScreen()
        {
            UF uf = new UF();
            cboUF.DataSource = uf.UFS();

            btnalterar.Enabled = false;
            btnalterar.Visible = false;
            lblalterar.Visible = false;
            dtpdatasaida.Enabled = false;
            dtpdatasaida.Visible = false;
            label7.Enabled = false;
            label7.Visible = false;
        }


        private void label21_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {


        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

            if (cboUF.SelectedIndex <=0 || cbosituacao.SelectedIndex <=0)
            {
                MessageBox.Show("Os campos UF e Situação não podem ficar vazios");
            }
            else
            {


                DTO_Fornecedores fornecedor = new DTO_Fornecedores();

                fornecedor.Nome_Fantasia = txtNFor.Text;
                fornecedor.CPF_CNPJ = txtcpfcnpjfornecedor.Text;
                fornecedor.Data_Cadastro = dtpdatacadastro.Value;
                //fornecedor.Data_Saida = dtpdatasaida.Value;
                fornecedor.Situacao = cbosituacao.SelectedItem.ToString();
                fornecedor.Data_Saida = dtpdatasaida.Value;
                fornecedor.observacao_Cadastro = txtobsCadastro.Text;
                fornecedor.CEP = txtCEP.Text;
                fornecedor.UF = cboUF.SelectedItem.ToString();
                fornecedor.Cidade = txtCidade.Text;
                fornecedor.Endereco = txtEndereço.Text;
                fornecedor.Complemento = txtcomplemento.Text;
                fornecedor.Bairro = txtBairro.Text;
                fornecedor.Numero = txtnumero.Text;
                fornecedor.Observacoes_Endereco = txtObsEndereco.Text;
                fornecedor.Telefone = txtTelefone.Text;
                fornecedor.Celular = txtCelular.Text;
                fornecedor.Email = txtEmail.Text;

                UF uf = new UF();
                cboUF.DataSource = uf.UFS();

                Business_Fornecedores business_Fornecedores = new Business_Fornecedores();
                business_Fornecedores.Salvar(fornecedor);

                MessageBox.Show("Fornecedor Cadastrado Com Sucesso!",
                                "Magic Buffet",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);



                Hide();

            }


        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Hide();
        }

        private void label25_Click(object sender, EventArgs e)
        {
            Hide();

        }

        private void label21_Click_1(object sender, EventArgs e)
        {

            this.WindowState = FormWindowState.Minimized;
        }
        DTO_Fornecedores dto;

        public void LoadScreenAlterar(DTO_Fornecedores dto)
        {
            this.dto = dto;
            lblID.Text = dto.ID_Fornecedor.ToString();
            txtNFor.Text = dto.Nome_Fantasia;
            txtcpfcnpjfornecedor.Text = dto.CPF_CNPJ;
            dtpdatacadastro.Value = dto.Data_Cadastro;
            dtpdatasaida.Value = dto.Data_Saida;
            cbosituacao.SelectedItem = dto.Situacao;

            txtobsCadastro.Text = dto.observacao_Cadastro;
            txtCEP.Text = dto.CEP;
            cboUF.SelectedItem = dto.UF;
            txtCidade.Text = dto.Cidade;
            txtEndereço.Text = dto.Endereco;
            txtcomplemento.Text = dto.Complemento;
            txtBairro.Text = dto.Bairro;
            txtnumero.Text = dto.Numero;
            txtObsEndereco.Text = dto.Observacoes_Endereco;
            txtTelefone.Text = dto.Telefone;
            txtCelular.Text = dto.Celular;
            txtEmail.Text = dto.Email;

            btnalterar.Enabled = true;
            btnalterar.Visible = true;
            lblalterar.Visible = true;
            btnsalvar.Enabled = false;
            btnsalvar.Visible = false;
            lblsalvar.Visible = false;
            dtpdatasaida.Enabled = true;
            dtpdatasaida.Visible = true;
                  label7.Enabled = true;
                  label7.Visible = true;
            lbltitulo.Text = "Alterar Fornecedor";
        }
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            dto.Nome_Fantasia = txtNFor.Text;
            dto.CPF_CNPJ = txtcpfcnpjfornecedor.Text;
            dto.Data_Cadastro = dtpdatacadastro.Value;
            dto.Data_Saida = dtpdatasaida.Value;
            dto.Situacao = cbosituacao.SelectedItem.ToString();
            dto.Data_Saida = dtpdatasaida.Value;
            dto.observacao_Cadastro = txtobsCadastro.Text;
            dto.CEP = txtCEP.Text;
            dto.UF = cboUF.SelectedItem.ToString();
            dto.Cidade = txtCidade.Text;
            dto.Endereco = txtEndereço.Text;
            dto.Complemento = txtcomplemento.Text;
            dto.Bairro = txtBairro.Text;
            dto.Numero = txtnumero.Text;
            dto.Observacoes_Endereco = txtObsEndereco.Text;
            dto.Telefone = txtTelefone.Text;
            dto.Celular = txtCelular.Text;
            dto.Email = txtEmail.Text;

            Business_Fornecedores business_Fornecedores = new Business_Fornecedores();
            business_Fornecedores.Alterar(dto);
            MessageBox.Show("Fornecedor Alterado com sucesso!",
                            "Magic Buffet",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            Hide();
        }

        private void CadastroFornecedores_Load(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void maskedTextBox2_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        
            private void ProcurarCEP()
        {
            string cep = txtCEP.Text;
            var service = new CorreiosApi();
            var dados = service.consultaCEP(cep);
            txtCidade.Text = dados.cidade;
            txtEndereço.Text = dados.end;
            txtBairro.Text = dados.bairro;
            cboUF.SelectedItem = dados.uf;



        
    }

        private void btncep_Click(object sender, EventArgs e)
        {
            ProcurarCEP();
        }
    }
}
