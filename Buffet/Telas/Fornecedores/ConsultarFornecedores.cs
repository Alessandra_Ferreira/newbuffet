﻿using Buffet.Db.Fornecedores;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class ConsultarFornecedores : Form
    {
        public ConsultarFornecedores()
        {
            InitializeComponent();
        }

        private void CarregarGried()
        {
            //Primeiro, devemos pegar os valores dos controles, pois posteriormente os mesmos serão utilizados para fazer a verificação no banco
            DTO_Fornecedores dto = new DTO_Fornecedores();
            dto.Nome_Fantasia = txtNome.Text;

            //Chamo a minha classe de négocio
            Business_Fornecedores db = new Business_Fornecedores();

            //Chamo o método de consultar passando o parâmetro dos valores encontrados nos controles
            List<DTO_Fornecedores> consult = db.Consultar(dto);

            //Configurando as colunas da gried view
            dgvFornecedores.AutoGenerateColumns = false;

            //Passando o valor do método
            dgvFornecedores.DataSource = consult;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void label21_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Telas.PedidoFornecedor ee = new Telas.PedidoFornecedor();
            ee.Show();
            Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Telas.Estoque oo = new Telas.Estoque();
            oo.Show();
            Hide();
        }

        //Evento de clikar em algum lugar da grid 
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Business_Fornecedores business = new Business_Fornecedores();
            //Se a coluna 5 -- 
            if (e.ColumnIndex == 4)
            {
                //pega os valores da linha selecionada e envia para o dto 
                DTO_Fornecedores dto = dgvFornecedores.CurrentRow.DataBoundItem as DTO_Fornecedores;
                business.Remover(dto.ID_Fornecedor);

                //Estilizada no método remover
                CarregarGried();
            }
            if (e.ColumnIndex == 5)
            {
                DTO_Fornecedores dto = dgvFornecedores.CurrentRow.DataBoundItem as DTO_Fornecedores;

                CadastroFornecedores tela = new CadastroFornecedores();
                tela.LoadScreenAlterar(dto);
                tela.Show();
            }
        }

        private void ConsultarFornecedores_Load(object sender, EventArgs e)
        {
            CarregarGried();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            CarregarGried();
        }

        private void dgvFornecedores_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
