﻿using Buffet.Db.Fornecedores;
using Buffet.Db.Fornecedores.Pedido_Fornecedor;
using Buffet.Db.Funcionarios;
using Buffet.Db.Pedido_Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Buffet.Telas
{
    public partial class PedidoFornecedor : Form
    {
        public PedidoFornecedor()
        {
            InitializeComponent();
            CarregarCombos();
        }

        void CarregarCombos()
        {
            Business_Fornecedores business = new Business_Fornecedores();
            List<DTO_Fornecedores> lista = business.Listar();

            cbofornecedores.ValueMember = nameof(DTO_Fornecedores.ID_Fornecedor);
            cbofornecedores.DisplayMember = nameof(DTO_Fornecedores.Nome_Fantasia);
            cbofornecedores.DataSource = lista;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            try
            {
                DTO_Fornecedores Fornecedor = cbofornecedores.SelectedItem as DTO_Fornecedores;

                DTO_Pedidofornecedor pedido = new DTO_Pedidofornecedor();
                //int fk = UserSession.UsuarioLogado.fk_funcionario;
                pedido.Nome_Produto = txtProduto.Text;
                pedido.Quantidade = Convert.ToInt32(txtqtd.Text);
                pedido.Valor_Unidade = Convert.ToDecimal(txtVU.Text);
                pedido.Valor_Total = Convert.ToDecimal(lblVT.Text);
                pedido.forma_Pagamento = cboForma.SelectedItem.ToString();
                pedido.Data_Pedido = DateTime.Now;
                pedido.fk_fornecedor = Fornecedor.ID_Fornecedor;

                Business_PedidoFornecedor business_PedidoFornecedor = new Business_PedidoFornecedor();
                business_PedidoFornecedor.Salvar(pedido);

                MessageBox.Show("Pedido realizado com sucesso!", "Magic Buffet", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Pedido_Fornecedor.ConsultarPedidos tela = new Pedido_Fornecedor.ConsultarPedidos();
                tela.Show();
                Hide();

            }
            catch
            {
                MessageBox.Show("Erro", "Magic Buffet", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Pedido atualizado com sucesso!", "Magic Buffet", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Telas.ConsultarFornecedores v = new Telas.ConsultarFornecedores();
            v.Show();
            Hide();
        }

        public void LoadScreenAlterar()
        {
            try
            {
                DTO_Pedidofornecedor dto = new DTO_Pedidofornecedor();
                dto.Nome_Produto = txtProduto.Text;
                dto.Quantidade = Convert.ToInt32(txtqtd.Text);
                dto.Valor_Unidade = Convert.ToDecimal(txtVU.Text);
                dto.Valor_Total = Convert.ToDecimal(lblVT.Text);
                dto.forma_Pagamento = cboForma.SelectedItem.ToString();
                dto.Data_Pedido = DateTime.Now;

                btnalterar.Enabled = true;
                btnsalvar.Enabled = false;
                lbltitulo.Text = "Alterar Fornecedor";

            }
            catch
            {
                MessageBox.Show("Erro", "Magic Buffet", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                int qtd = Convert.ToInt32(txtqtd.Text);
                decimal vlUnidade = Convert.ToDecimal(txtVU.Text);
                CalculoPedidoFornecedor calculoPedidoFornecedor = new CalculoPedidoFornecedor();
                decimal result = calculoPedidoFornecedor.Calcular(qtd, vlUnidade);

                lblVT.Text = result.ToString();
            }
            catch
            {
                MessageBox.Show("Erro", "Magic Buffet", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
            }
        }

        private void label21_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            Telas.Pedido_Fornecedor.ConsultarPedidos tela = new Pedido_Fornecedor.ConsultarPedidos();
            tela.Show();
            Hide();
        }
    }
}
