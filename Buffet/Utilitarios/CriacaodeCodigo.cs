﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Utilitarios
{
    class CriacaodeCodigo
    {
        public string Codigo4()
        {
            string[] cod = new string[5];
            Random random = new Random();
            string numero = string.Empty;


            for (int i = 0; i < 4; i++)
            {
                switch (random.Next(1, 62))
                {
                    case 1:
                        cod[i] = "a";
                        break;
                    case 2:
                        cod[i] = "b";
                        break;
                    case 3:
                        cod[i] = "c";
                        break;
                    case 4:
                        cod[i] = "d";
                        break;
                    case 5:
                        cod[i] = "e";
                        break;
                    case 6:
                        cod[i] = "f";
                        break;
                    case 7:
                        cod[i] = "g";
                        break;
                    case 8:
                        cod[i] = "h";
                        break;
                    case 9:
                        cod[i] = "i";
                        break;
                    case 10:
                        cod[i] = "j";
                        break;
                    case 11:
                        cod[i] = "k";
                        break;
                    case 12:
                        cod[i] = "l";
                        break;
                    case 13:
                        cod[i] = "m";
                        break;
                    case 14:
                        cod[i] = "n";
                        break;
                    case 15:
                        cod[i] = "o";
                        break;
                    case 16:
                        cod[i] = "p";
                        break;
                    case 17:
                        cod[i] = "q";
                        break;
                    case 18:
                        cod[i] = "r";
                        break;
                    case 19:
                        cod[i] = "s";
                        break;
                    case 20:
                        cod[i] = "t";
                        break;
                    case 21:
                        cod[i] = "u";
                        break;
                    case 22:
                        cod[i] = "v";
                        break;
                    case 23:
                        cod[i] = "w";
                        break;
                    case 24:
                        cod[i] = "x";
                        break;
                    case 25:
                        cod[i] = "y";
                        break;
                    case 26:
                        cod[i] = "z";
                        break;
                    case 27:
                        cod[i] = "A";
                        break;
                    case 28:
                        cod[i] = "B";
                        break;
                    case 29:
                        cod[i] = "C";
                        break;
                    case 30:
                        cod[i] = "D";
                        break;
                    case 31:
                        cod[i] = "E";
                        break;
                    case 32:
                        cod[i] = "F";
                        break;
                    case 33:
                        cod[i] = "G";
                        break;
                    case 34:
                        cod[i] = "H";
                        break;
                    case 35:
                        cod[i] = "I";
                        break;
                    case 36:
                        cod[i] = "J";
                        break;
                    case 37:
                        cod[i] = "K";
                        break;
                    case 38:
                        cod[i] = "L";
                        break;
                    case 39:
                        cod[i] = "M";
                        break;
                    case 40:
                        cod[i] = "N";
                        break;
                    case 41:
                        cod[i] = "O";
                        break;
                    case 42:
                        cod[i] = "P";
                        break;
                    case 43:
                        cod[i] = "Q";
                        break;
                    case 44:
                        cod[i] = "R";
                        break;
                    case 45:
                        cod[i] = "S";
                        break;
                    case 46:
                        cod[i] = "T";
                        break;
                    case 47:
                        cod[i] = "U";
                        break;
                    case 48:
                        cod[i] = "V";
                        break;
                    case 49:
                        cod[i] = "W";
                        break;
                    case 50:
                        cod[i] = "X";
                        break;
                    case 51:
                        cod[i] = "Y";
                        break;
                    case 52:
                        cod[i] = "Z";
                        break;
                    case 53:
                        cod[i] = "0";
                        break;
                    case 54:
                        cod[i] = "1";
                        break;
                    case 55:
                        cod[i] = "2";
                        break;
                    case 56:
                        cod[i] = "3";
                        break;
                    case 57:
                        cod[i] = "4";
                        break;
                    case 58:
                        cod[i] = "5";
                        break;
                    case 59:
                        cod[i] = "6";
                        break;
                    case 60:
                        cod[i] = "7";
                        break;
                    case 61:
                        cod[i] = "8";
                        break;
                    case 62:
                        cod[i] = "9";
                        break;
                }
            }


            string codigo = cod[0] + cod[1] + cod[2] + cod[3] + cod[4];
            return codigo;
        }

    }
}
