﻿using Buffet.Db.FolhaDePagamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Utilitarios
{
    public class CalcularFolhadePagamento
    {
       
       
            public double SalarioLiquido,
                        MesValor,
                        ValorHora,
                        ValorHora50,
                        ValorHora100,
                        ValorTrab50,
                        ValorTrab100,
                        ValorINSS,
                        IR,
                        DeducaoIR,
                        ValorIR,
                        ValorFGTS,
                        ValorValeRefeicao,
                        ValorValeTransposte,
                        ToltalDeProventos,
                        TotalDeDescontos;


        DTO_FolhaDePagamento dto = new DTO_FolhaDePagamento();

        public double Calculo(double salariobruto, int DiasTrabMes, int numerodediasdomes, int HorasTrab50, int HorasTrab100,int idfuncionario)
            {

                //Mes/Valor
                MesValor = (salariobruto / numerodediasdomes) * DiasTrabMes;

                //Valor hora 
                ValorHora = salariobruto / 220;

                //Valor/hora 50%
                ValorHora50 = ValorHora + (ValorHora * 0.5);

                //Valor/hora 100%
                ValorHora100 = ValorHora + (ValorHora * 1);

                //Valor trab 50%
                ValorTrab50 = HorasTrab50 * ValorHora50;

                //Valor trab 100%
                ValorHora100 = HorasTrab100 * ValorHora100;

                //valor INSS
                if (salariobruto <= 1693.73)
                {
                    ValorINSS = salariobruto * 0.08;
                }
           else if (salariobruto > 1693.73 && salariobruto < 2822.91
    )
                {
                    ValorINSS = salariobruto * 0.09;
                }
                else if (salariobruto >= 2822.91)
                {
                    ValorINSS = salariobruto * 0.11;
                }

                //IR - coluna de aliquota 
                if (salariobruto == 0)
                {
                dto.fi_faixa_irrf = 0;
                

                    IR = salariobruto * 0;                
                        DeducaoIR = 0;
                dto.bs_base_calc_irrf = DeducaoIR;
                    // Valor ir 
                    ValorIR = IR - DeducaoIR;

                }
                else if (salariobruto < 1903.98 && salariobruto > 0)
                {
                dto.fi_faixa_irrf = 0.075;


                        IR = salariobruto * 0.075;
                    DeducaoIR = 142.80;
                dto.bs_base_calc_irrf = DeducaoIR;

                // Valor ir 
                ValorIR = IR - DeducaoIR;

                }
                else if (salariobruto == 2826.65)
                {
                dto.fi_faixa_irrf = 0.15;

                IR = salariobruto * 0.15;
                    DeducaoIR = 354.80;
                dto.bs_base_calc_irrf = DeducaoIR;

                // Valor ir 
                ValorIR = IR - DeducaoIR;

                }
                else if (salariobruto == 3751.05)
                {
                dto.fi_faixa_irrf = 0.02250;

                IR = salariobruto * 0.02250;
                    DeducaoIR = 636.13;
                dto.bs_base_calc_irrf = DeducaoIR;

                // Valor ir 
                ValorIR = IR - DeducaoIR;

                }
                else if (salariobruto == 4664.68)
                {
                dto.fi_faixa_irrf = 0.0275;

                IR = salariobruto * 0.275;
                    DeducaoIR = 869.36;
                dto.bs_base_calc_irrf = DeducaoIR;

                // Valor ir 
                ValorIR = IR - DeducaoIR;

                }

                else { }

                //Valor fgts
                ValorFGTS = salariobruto * 0.08;

                // Valor vale refeição
                ValorValeRefeicao = salariobruto * 0.08;

                //Valor Vale transporte 
                ValorValeTransposte = salariobruto * 0.06;

                //Valor de proventos 
                ToltalDeProventos = MesValor + ValorTrab50 + ValorTrab100 + DeducaoIR;

                // valor total de descontos 
                TotalDeDescontos = ValorINSS + IR + ValorIR + ValorFGTS + ValorValeRefeicao + ValorValeTransposte;

                //valor salario liquido 
                SalarioLiquido = ToltalDeProventos - TotalDeDescontos;

            dto.sl_salario_mes = MesValor;
            dto.vl_vale_trasporte = ValorValeTransposte;
            dto.vl_vale_refeicao = ValorValeRefeicao;
            dto.ad_adiantamento = 0;
            dto.inss = ValorINSS;
            dto.hr_hora_extra = Convert.ToDateTime(HorasTrab50);
            dto.ds_descontos = TotalDeDescontos;
            dto.sl_salario_base = salariobruto;
            dto.cl_calculo_base_inss = ValorINSS;
            dto.cl_cauculo_base_fgts = ValorFGTS;
            dto.ft_fgts_do_mes = ValorFGTS;
            dto.bs_base_calc_irrf = ValorIR;
            dto.tl_total_vencimentos = 0;
            dto.fi_faixa_irrf = 0;
            dto.tl_total_vencimentos = 0;
            dto.rf_referencia = 0;
            dto.ms_mensagem = null;
            dto.sl_salario_bruto = salariobruto;
            dto.pl_plano_de_saude = 0;
            dto.hr_horas_trabalhadas = Convert.ToDateTime(HorasTrab100);
            dto.sl_hora = ValorHora100;
            //não pode ser datetime
            //dto.dias_trabalhados = numerodediasdomes;
            dto.dt_datapagamento = DateTime.Now;
            dto.fk_id_funcionario_fp = idfuncionario;

            Business_FolhaDePagamento bus = new Business_FolhaDePagamento();
            bus.Salvar(dto);
                return SalarioLiquido;
            }

        

    }
   
}
