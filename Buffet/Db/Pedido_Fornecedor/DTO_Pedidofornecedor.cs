﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Fornecedores.Pedido_Fornecedor
{
    class DTO_Pedidofornecedor
    {

        public int ID { get; set; }
        public string Nome_Produto { get; set; }
        public int Quantidade { get; set; }
        public DateTime Data_Pedido { get; set; }
        public decimal Valor_Unidade { get; set; }
        public decimal Valor_Total { get; set; }
        public string forma_Pagamento { get; set; }
 
        public int fk_fornecedor { get; set; }
    }
}
