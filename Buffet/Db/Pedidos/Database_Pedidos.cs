﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Pedidos
{
    class Database_Pedidos
    {
        public int Salvar(DTO_Pedidos dto)
        {
            string script = @"Insert into   pedido_festa ( 
                                                                fk_cliente,
                                                                fk_agenda,
                                                             Data_Agendamento, 
                                                             Descricao, 
                                                             local , 
                                                             data_Festa ,
                                                             Valor_Total,        
                                                             Valor,              
                                                             desconto,
                                                             forma_pagamento
    
                                                                ) Values (
                                                                    @fk_cliente,
                                                                    @fk_agenda,
                                                                    @Data_Agendamento,
                                                                    @Descricao, 
                                                                    @local,
                                                                    @data_Festa,
                                                                    @Valor_Total,
                                                                    @Valor,
                                                                    @desconto,
                                                                    @forma_Pagamento)  ";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("fk_cliente", dto.fk_cliente));
            parm.Add(new MySqlParameter("fk_agenda", dto.fk_agenda));
            parm.Add(new MySqlParameter("Descricao", dto.descricao));
            parm.Add(new MySqlParameter("local", dto.local));
            parm.Add(new MySqlParameter("Valor_Total", dto.Valor_Total));
            parm.Add(new MySqlParameter("Valor", dto.Valor));
            parm.Add(new MySqlParameter("desconto", dto.desconto));
            parm.Add(new MySqlParameter("forma_Pagamento", dto.forma_Pagamento));

            Database db = new Database();

            return db.ExecuteInsertScriptWithPk(script, parm);


        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM pedido_festa WHERE ID_PedidoFesta = @ID_PedidoFesta";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_PedidoFesta", id));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(DTO_Pedidos dto)
        {
            string script = @" update pedido_festa Set 
                                                        fk_cliente = @fk_cliente,
                                                        fk_agenda = @fk_agenda,
                                                      
                                                        Descricao = @Descricao,
                                                        local = @local,
                                                        
                                                        Valor_Total = @Valor_Total, 
                                                        Valor = @Valor,
                                                        desconto = @desconto,
                                                        forma_Pagamento = @pagamento";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("fk_cliente", dto.fk_cliente));
            parm.Add(new MySqlParameter("fk_agenda", dto.fk_agenda));
            parm.Add(new MySqlParameter("Descricao", dto.descricao));
            parm.Add(new MySqlParameter("local", dto.local));
            parm.Add(new MySqlParameter("Valor_Total", dto.Valor_Total));
            parm.Add(new MySqlParameter("Valor", dto.Valor));
            parm.Add(new MySqlParameter("desconto", dto.desconto));
            parm.Add(new MySqlParameter("forma_Pagamento", dto.forma_Pagamento));

            Database db = new Database();
            db.ExecuteSelectScript(script, parm);



        }


        public List<DTO_Pedidos> List()
        {
            string script = @"Select *  from Pedido_Festa";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Pedidos> lista = new List<DTO_Pedidos>();
            while (reader.Read())
            {

                DTO_Pedidos dto = new DTO_Pedidos();
                dto.id = reader.GetInt32("id");
                dto.fk_cliente = reader.GetInt32("fk_cliente");
                dto.fk_agenda = reader.GetInt32("fk_agenda");
                dto.desconto = reader.GetDecimal("desconto");
                dto.descricao = reader.GetString("Descricao");
                dto.forma_Pagamento = reader.GetString("forma_Pagamento");
                dto.Valor = reader.GetDecimal("Valor");
                dto.Valor_Total = reader.GetDecimal("Valor_Total");
                dto.local = reader.GetString("local");

                lista.Add(dto);



            }
            reader.Close();
            return lista;


        }

        public List<DTO_Consultar_Pedido> ConsultarPedidos (string nome)
        {
            string script = @"select * from consultar_pedidos where Nome Like @Nome";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", nome + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Consultar_Pedido> lista = new List<DTO_Consultar_Pedido>();
            while(reader.Read())
            {
                DTO_Consultar_Pedido dto = new DTO_Consultar_Pedido();
                dto.NomeCliente = reader.GetString("Nome");
                dto.DataAgendamento = reader.GetDateTime("dt_dia_do_agendamento");
                dto.DataFesta = reader.GetDateTime("dt_data");
                dto.LocaldaFesta = reader.GetString("Local_Festa");

                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

    }
}