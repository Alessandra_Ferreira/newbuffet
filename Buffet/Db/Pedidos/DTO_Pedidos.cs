﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Pedidos
{
    public class DTO_Pedidos

    {

        public int id { get; set; }
        public int fk_cliente { get; set; }
        public int fk_agenda { get; set; }
        public DateTime dt_agendamento { get; set; }
        public string descricao { get; set; }
        public string local { get; set; }
        public decimal Valor_Total { get; set; }
        public decimal Valor { get; set; }
        public decimal desconto { get; set; }
        public string forma_Pagamento { get; set; }

        


    }
}
