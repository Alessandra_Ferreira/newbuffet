﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Funcionarios
{
   public  class Database_Funcionario
    {
        public int Salvar(DTO_Fucionario dto)
        {
            string script = @"INSERT INTO funcionario (Nome, CPF, Data_Cadastro, Situacao, Data_Saida, Cargo, Observacoes_Cadastro, UF, Cidade, CEP, Endereco,
                numero, Complemento, Bairro, Observacoes_Endereco,Telefone, Celular, Email, pr_permissao_cliente,
                                                                                         pr_permissao_cadastrar_cliente ,
                                                                                         pr_permissao_consultar_cliente ,
                                                                                         pr_permissao_atualiza_cadastror_cliente ,
                                                                                         pr_permissao_pedido, 
                                                                                         pr_permissao_novo_pedido ,
                                                                                         pr_permissao_atualizar_pedido,
                                                                                         pr_permissao_consultar_pedido ,
                                                                                         pr_permissao_funcionario ,
                                                                                         pr_permissao_cadastrar_funcionario ,
                                                                                         pr_permissao_novo_funcionario ,
                                                                                         pr_permissao_alterar_dados_funcionarios ,
                                                                                         pr_permissao_consultar_funcionarios ,
                                                                                         pr_permissao_fornecedor ,
                                                                                         pr_permissao_cadastrar_fornecedor ,
                                                                                         pr_permissao_consultar_fornecedor ,
                                                                                         pr_permissao_estoque ,
                                                                                         pr_permissao_consultar_estoque ,
                                                                                         pr_permissao_alterar_estoque ,
                                                                                         pr_permissao_perdir_ao_fornecedor ,
                                                                                         pr_permissao_cadastrar_terceirizados ,
                                                                                         pr_permissao_consultar_terceriziado ,
                                                                                         pr_permisssao_terceirizados,
                                                                                         user_usuario,
                                                                                         sn_senha) 

                VALUES (@Nome, @CPF, @Data_Cadastro, @Situacao, @Data_Saida, @Cargo, @Observacoes_Cadastro, @UF, @Cidade, @CEP, @Endereco, @numero,
                @Complemento, @Bairro, @Observacoes_Endereco, @Telefone, @Celular, @Email, pr_permissao_cliente,
                                                                                        @pr_permissao_cadastrar_cliente ,
                                                                                        @pr_permissao_consultar_cliente ,
                                                                                        @pr_permissao_atualiza_cadastror_cliente ,
                                                                                        @pr_permissao_pedido, 
                                                                                        @pr_permissao_novo_pedido ,
                                                                                        @pr_permissao_atualizar_pedido,
                                                                                        @pr_permissao_consultar_pedido ,
                                                                                        @pr_permissao_funcionario ,
                                                                                        @pr_permissao_cadastrar_funcionario ,
                                                                                        @pr_permissao_novo_funcionario ,
                                                                                        @pr_permissao_alterar_dados_funcionarios ,
                                                                                        @pr_permissao_consultar_funcionarios ,
                                                                                        @pr_permissao_fornecedor ,
                                                                                        @pr_permissao_cadastrar_fornecedor ,
                                                                                        @pr_permissao_consultar_fornecedor ,
                                                                                        @pr_permissao_estoque ,
                                                                                        @pr_permissao_consultar_estoque ,
                                                                                        @pr_permissao_alterar_estoque ,
                                                                                        @pr_permissao_perdir_ao_fornecedor ,
                                                                                        @pr_permissao_cadastrar_terceirizados ,
                                                                                        @pr_permissao_consultar_terceriziado ,
                                                                                        @pr_permisssao_terceirizados , @user_usuario,
                                                                                        @sn_senha)";



            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parms.Add(new MySqlParameter("situacao", dto.Situacao));
            parms.Add(new MySqlParameter("Data_Saida", dto.Data_Saida));
            parms.Add(new MySqlParameter("Cargo", dto.cargo));
            parms.Add(new MySqlParameter("Observacoes_Cadastro", dto.observacao_Cadastro));
            parms.Add(new MySqlParameter("UF", dto.UF));
            parms.Add(new MySqlParameter("Cidade", dto.cidade));
            parms.Add(new MySqlParameter("CEP", dto.CEP));
            parms.Add(new MySqlParameter("Endereco", dto.endereco));
            parms.Add(new MySqlParameter("numero", dto.Numero));
            parms.Add(new MySqlParameter("Complemento", dto.complemento));
            parms.Add(new MySqlParameter("Bairro", dto.bairro));
            parms.Add(new MySqlParameter("Observacoes_Endereco", dto.observacoes_Endereco));
            parms.Add(new MySqlParameter("Telefone", dto.telefone));
            parms.Add(new MySqlParameter("Celular", dto.celular));
            parms.Add(new MySqlParameter("Email", dto.email));

            parms.Add(new MySqlParameter("pr_permissao_cliente", dto.pr_permissao_cliente));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_cliente", dto.pr_permissao_cadastrar_cliente));
            parms.Add(new MySqlParameter("pr_permissao_consultar_cliente", dto.pr_permissao_consultar_cliente));
            parms.Add(new MySqlParameter("pr_permissao_atualiza_cadastror_cliente", dto.pr_permissao_atualiza_cadastror_cliente));
            parms.Add(new MySqlParameter("pr_permissao_pedido", dto.pr_permissao_pedido));
            parms.Add(new MySqlParameter("pr_permissao_novo_pedido", dto.pr_permissao_novo_pedido));
            parms.Add(new MySqlParameter("pr_permissao_atualizar_pedido", dto.pr_permissao_atualizar_pedido));
            parms.Add(new MySqlParameter("pr_permissao_consultar_pedido", dto.pr_permissao_consultar_pedido));
            parms.Add(new MySqlParameter("pr_permissao_funcionario", dto.pr_permissao_funcionario));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_funcionario", dto.pr_permissao_cadastrar_funcionario));
            parms.Add(new MySqlParameter("pr_permissao_novo_funcionario", dto.pr_permissao_novo_funcionario));
            parms.Add(new MySqlParameter("pr_permissao_alterar_dados_funcionarios", dto.pr_permissao_alterar_dados_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_consultar_funcionarios", dto.pr_permissao_consultar_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_fornecedor", dto.pr_permissao_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_fornecedor", dto.pr_permissao_cadastrar_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_consultar_fornecedor", dto.pr_permissao_consultar_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_estoque", dto.pr_permissao_estoque));
            parms.Add(new MySqlParameter("pr_permissao_consultar_estoque", dto.pr_permissao_consultar_estoque));
            parms.Add(new MySqlParameter("pr_permissao_alterar_estoque", dto.pr_permissao_alterar_estoque));

            parms.Add(new MySqlParameter("pr_permissao_perdir_ao_fornecedor", dto.pr_permissao_perdir_ao_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_terceirizados", dto.pr_permissao_cadastrar_terceiriziados));
            parms.Add(new MySqlParameter("pr_permissao_consultar_terceriziado", dto.pr_permissao_consultar_terceriziado));
            parms.Add(new MySqlParameter("pr_permisssao_terceirizados", dto.pr_permisssao_terceirizados));

            parms.Add(new MySqlParameter("user_usuario", dto.user_usuario));
            parms.Add(new MySqlParameter("sn_senha", dto.sn_senha));
            


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(DTO_Fucionario dto)
        {
            string script = @"UPDATE funcionario 
                                 SET Nome = @Nome,
                                     CPF   = @CPF,
                                       
                               WHERE ID_Funcionario = @ID_Funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Funcionario", dto.ID_Funcionario));
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parms.Add(new MySqlParameter("situacao", dto.Situacao));
            parms.Add(new MySqlParameter("Data_Saida", dto.Data_Saida));
            parms.Add(new MySqlParameter("Observacoes_Cadastro", dto.observacao_Cadastro));
            parms.Add(new MySqlParameter("UF", dto.UF));
            parms.Add(new MySqlParameter("Cidade", dto.cidade));
            parms.Add(new MySqlParameter("CEP", dto.CEP));
            parms.Add(new MySqlParameter("Endereco", dto.endereco));
            parms.Add(new MySqlParameter("numero", dto.Numero));
            parms.Add(new MySqlParameter("Complemento", dto.complemento));
            parms.Add(new MySqlParameter("Bairro", dto.bairro));
            parms.Add(new MySqlParameter("Observacoes_Endereco", dto.observacoes_Endereco));
            parms.Add(new MySqlParameter("Telefone", dto.telefone));
            parms.Add(new MySqlParameter("Celular", dto.celular));
            parms.Add(new MySqlParameter("Email", dto.email));

            parms.Add(new MySqlParameter("pr_permissao_cliente", dto.pr_permissao_cliente));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_cliente", dto.pr_permissao_cadastrar_cliente));
            parms.Add(new MySqlParameter("pr_permissao_consultar_cliente", dto.pr_permissao_consultar_cliente));
            parms.Add(new MySqlParameter("pr_permissao_atualiza_cadastror_cliente", dto.pr_permissao_atualiza_cadastror_cliente));
            parms.Add(new MySqlParameter("pr_permissao_pedido", dto.pr_permissao_pedido));
            parms.Add(new MySqlParameter("pr_permissao_novo_pedido", dto.pr_permissao_novo_pedido));
            parms.Add(new MySqlParameter("pr_permissao_atualizar_pedido", dto.pr_permissao_atualizar_pedido));
            parms.Add(new MySqlParameter("pr_permissao_consultar_pedido", dto.pr_permissao_consultar_pedido));
            parms.Add(new MySqlParameter("pr_permissao_funcionario", dto.pr_permissao_funcionario));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_funcionario", dto.pr_permissao_cadastrar_funcionario));
            parms.Add(new MySqlParameter("pr_permissao_novo_funcionario", dto.pr_permissao_novo_funcionario));
            parms.Add(new MySqlParameter("pr_permissao_alterar_dados_funcionarios", dto.pr_permissao_alterar_dados_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_consultar_funcionarios", dto.pr_permissao_consultar_funcionarios));
            parms.Add(new MySqlParameter("pr_permissao_fornecedor", dto.pr_permissao_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_fornecedor", dto.pr_permissao_cadastrar_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_consultar_fornecedor", dto.pr_permissao_consultar_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_estoque", dto.pr_permissao_estoque));
            parms.Add(new MySqlParameter("pr_permissao_consultar_estoque", dto.pr_permissao_consultar_estoque));
            parms.Add(new MySqlParameter("pr_permissao_alterar_estoque", dto.pr_permissao_alterar_estoque));

            parms.Add(new MySqlParameter("pr_permissao_perdir_ao_fornecedor", dto.pr_permissao_perdir_ao_fornecedor));
            parms.Add(new MySqlParameter("pr_permissao_cadastrar_terceirizados", dto.pr_permissao_cadastrar_terceiriziados));
            parms.Add(new MySqlParameter("pr_permissao_consultar_terceriziado", dto.pr_permissao_consultar_terceriziado));
            parms.Add(new MySqlParameter("pr_permisssao_terceirizados", dto.pr_permisssao_terceirizados));


            
            parms.Add(new MySqlParameter("user_usuario", dto.user_usuario));
            parms.Add(new MySqlParameter("sn_senha", dto.sn_senha));

            Database db = new Database();
            db.ExecuteSelectScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_Funcionario WHERE ID_Funcionario = @ID_Funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Funcionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_Fucionario> Listar()
        {
            string script = @"SELECT * FROM funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Fucionario> lista = new List<DTO_Fucionario>();
            while (reader.Read())
            {
                DTO_Fucionario dto = new DTO_Fucionario();
                dto.ID_Funcionario = reader.GetInt32("ID_Funcionario");
                dto.Nome = reader.GetString("Nome");
                dto.CPF = reader.GetString("CPF");
                dto.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                dto.Situacao = reader.GetString("Situacao");
                dto.Data_Saida = reader.GetDateTime("Data_Saida");
                dto.cargo = reader.GetString("Cargo");
                dto.observacao_Cadastro = reader.GetString("Observacoes_Cadastro");
                dto.CEP = reader.GetString("CEP");
                dto.UF = reader.GetString("UF");
                dto.cidade = reader.GetString("Cidade");
                dto.endereco = reader.GetString("Endereco");

                dto.complemento = reader.GetString("CPF");

                dto.bairro = reader.GetString("Bairro");
                dto.Numero = reader.GetString("numero");
                dto.observacoes_Endereco = reader.GetString("Observacoes_Endereco");


                dto.telefone = reader.GetString("Telefone");
                dto.celular = reader.GetString("Celular");
                dto.email = reader.GetString("Email");

                dto.pr_permissao_cliente = reader.GetBoolean("pr_permissao_cliente");
                dto.pr_permissao_cadastrar_cliente = reader.GetBoolean("pr_permissao_cadastrar_cliente");
                dto.pr_permissao_consultar_cliente = reader.GetBoolean("pr_permissao_consultar_cliente");
                dto.pr_permissao_atualiza_cadastror_cliente = reader.GetBoolean("pr_permissao_atualiza_cadastror_cliente");
                dto.pr_permissao_pedido = reader.GetBoolean("pr_permissao_pedido");
                dto.pr_permissao_novo_pedido = reader.GetBoolean("pr_permissao_novo_pedido");
                dto.pr_permissao_atualizar_pedido = reader.GetBoolean("pr_permissao_atualizar_pedido");
                dto.pr_permissao_consultar_pedido = reader.GetBoolean("pr_permissao_consultar_pedido");
                dto.pr_permissao_funcionario = reader.GetBoolean("pr_permissao_funcionario");
                dto.pr_permissao_cadastrar_funcionario = reader.GetBoolean("pr_permissao_cadastrar_funcionario");
                dto.pr_permissao_novo_funcionario = reader.GetBoolean("pr_permissao_novo_funcionario");
                dto.pr_permissao_alterar_dados_funcionarios = reader.GetBoolean("pr_permissao_alterar_dados_funcionarios");
                dto.pr_permissao_consultar_funcionarios = reader.GetBoolean("pr_permissao_consultar_funcionarios");
                dto.pr_permissao_fornecedor = reader.GetBoolean("pr_permissao_fornecedor");
                dto.pr_permissao_cadastrar_fornecedor = reader.GetBoolean("pr_permissao_cadastrar_fornecedor");
                dto.pr_permissao_consultar_fornecedor = reader.GetBoolean("pr_permissao_consultar_fornecedor");
                dto.pr_permissao_estoque = reader.GetBoolean("pr_permissao_estoque");
                dto.pr_permissao_consultar_estoque = reader.GetBoolean("pr_permissao_consultar_estoque");
                dto.pr_permissao_alterar_estoque = reader.GetBoolean("pr_permissao_alterar_estoque");
                dto.pr_permissao_perdir_ao_fornecedor = reader.GetBoolean("pr_permissao_perdir_ao_fornecedor");
                dto.pr_permissao_cadastrar_terceiriziados = reader.GetBoolean("pr_permissao_cadastrar_terceirizados");
                dto.pr_permissao_consultar_terceriziado = reader.GetBoolean("pr_permissao_consultar_terceriziado");
                dto.pr_permisssao_terceirizados = reader.GetBoolean("pr_permisssao_terceirizados");

                dto.sn_senha = reader.GetString("sn_senha");
                dto.user_usuario = reader.GetString("user_usuario");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<DTO_Fucionario> Consultar(string nome)
        {
            string script = @"SELECT * FROM funcionario WHERE Nome like @Nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Fucionario> lista = new List<DTO_Fucionario>();
            while (reader.Read())
            {
                DTO_Fucionario dto = new DTO_Fucionario();
                dto.ID_Funcionario = reader.GetInt32("ID_Funcionario");
                dto.Nome = reader.GetString("Nome");
                dto.CPF = reader.GetString("CPF");
                dto.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                dto.Situacao = reader.GetString("Situacao");
                dto.Data_Saida = reader.GetDateTime("Data_Saida");
                dto.cargo = reader.GetString("Cargo");
                dto.observacao_Cadastro = reader.GetString("Observacoes_Cadastro");
                dto.CEP = reader.GetString("CEP");
                dto.UF = reader.GetString("UF");
                dto.cidade = reader.GetString("Cidade");
                dto.endereco = reader.GetString("Endereco");

                dto.complemento = reader.GetString("CPF");

                dto.bairro = reader.GetString("Bairro");
                dto.Numero = reader.GetString("numero");
                dto.observacoes_Endereco = reader.GetString("Observacoes_Endereco");


                dto.telefone = reader.GetString("Telefone");
                dto.celular = reader.GetString("Celular");
                dto.email = reader.GetString("Email");

                dto.pr_permissao_cliente = reader.GetBoolean("pr_permissao_cliente");
                dto.pr_permissao_cadastrar_cliente = reader.GetBoolean("pr_permissao_cadastrar_cliente");
                dto.pr_permissao_consultar_cliente = reader.GetBoolean("pr_permissao_consultar_cliente");
                dto.pr_permissao_atualiza_cadastror_cliente = reader.GetBoolean("pr_permissao_atualiza_cadastror_cliente");
                dto.pr_permissao_pedido = reader.GetBoolean("pr_permissao_pedido");
                dto.pr_permissao_novo_pedido = reader.GetBoolean("pr_permissao_novo_pedido");
                dto.pr_permissao_atualizar_pedido = reader.GetBoolean("pr_permissao_atualizar_pedido");
                dto.pr_permissao_consultar_pedido = reader.GetBoolean("pr_permissao_consultar_pedido");
                dto.pr_permissao_funcionario = reader.GetBoolean("pr_permissao_funcionario");
                dto.pr_permissao_cadastrar_funcionario = reader.GetBoolean("pr_permissao_cadastrar_funcionario");
                dto.pr_permissao_novo_funcionario = reader.GetBoolean("pr_permissao_novo_funcionario");
                dto.pr_permissao_alterar_dados_funcionarios = reader.GetBoolean("pr_permissao_alterar_dados_funcionarios");
                dto.pr_permissao_consultar_funcionarios = reader.GetBoolean("pr_permissao_consultar_funcionarios");
                dto.pr_permissao_fornecedor = reader.GetBoolean("pr_permissao_fornecedor");
                dto.pr_permissao_cadastrar_fornecedor = reader.GetBoolean("pr_permissao_cadastrar_fornecedor");
                dto.pr_permissao_consultar_fornecedor = reader.GetBoolean("pr_permissao_consultar_fornecedor");
                dto.pr_permissao_estoque = reader.GetBoolean("pr_permissao_estoque");
                dto.pr_permissao_consultar_estoque = reader.GetBoolean("pr_permissao_consultar_estoque");
                dto.pr_permissao_alterar_estoque = reader.GetBoolean("pr_permissao_alterar_estoque");
                dto.pr_permissao_perdir_ao_fornecedor = reader.GetBoolean("pr_permissao_perdir_ao_fornecedor");
                dto.pr_permissao_cadastrar_terceiriziados = reader.GetBoolean("pr_permissao_cadastrar_terceirizados");
                dto.pr_permissao_consultar_terceriziado = reader.GetBoolean("pr_permissao_consultar_terceriziado");
                dto.pr_permisssao_terceirizados = reader.GetBoolean("pr_permisssao_terceirizados");

                dto.sn_senha = reader.GetString("sn_senha");
                dto.user_usuario = reader.GetString("user_usuario");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public DTO_Fucionario Logar(string usuario, string senha)
        {
            string script = @"select * from funcionario where 
                                                                 user_usuario= @user_usuario and
                                                                 sn_senha= @sn_senha";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("user_usuario", usuario));
            parms.Add(new MySqlParameter("sn_senha", senha));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            DTO_Fucionario dto = null;
            while (reader.Read())
            {
                dto = new DTO_Fucionario();
                dto.ID_Funcionario = reader.GetInt32("ID_Funcionario");
                dto.user_usuario = reader.GetString("user_usuario");
                dto.sn_senha = reader.GetString("sn_senha");
                dto.Nome = reader.GetString("Nome");
                dto.CPF = reader.GetString("CPF");
                dto.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                dto.Situacao = reader.GetString("Situacao");
                dto.Data_Saida = reader.GetDateTime("Data_Saida");
                dto.cargo = reader.GetString("Cargo");
                dto.observacao_Cadastro = reader.GetString("Observacoes_Cadastro");
                dto.CEP = reader.GetString("CEP");
                dto.UF = reader.GetString("UF");
                dto.cidade = reader.GetString("Cidade");
                dto.endereco = reader.GetString("Endereco");
                dto.complemento = reader.GetString("CPF");
                dto.bairro = reader.GetString("Bairro");
                dto.Numero = reader.GetString("numero");
                dto.observacoes_Endereco = reader.GetString("Observacoes_Endereco");
                dto.telefone = reader.GetString("Telefone");
                dto.celular = reader.GetString("Celular");
                dto.email = reader.GetString("Email");
                dto.pr_permissao_cliente = reader.GetBoolean("pr_permissao_cliente");
                dto.pr_permissao_cadastrar_cliente = reader.GetBoolean("pr_permissao_cadastrar_cliente");
                dto.pr_permissao_consultar_cliente = reader.GetBoolean("pr_permissao_consultar_cliente");
                dto.pr_permissao_atualiza_cadastror_cliente = reader.GetBoolean("pr_permissao_atualiza_cadastror_cliente");
                dto.pr_permissao_pedido = reader.GetBoolean("pr_permissao_pedido");
                dto.pr_permissao_novo_pedido = reader.GetBoolean("pr_permissao_novo_pedido");
                dto.pr_permissao_atualizar_pedido = reader.GetBoolean("pr_permissao_atualizar_pedido");
                dto.pr_permissao_consultar_pedido = reader.GetBoolean("pr_permissao_consultar_pedido");
                dto.pr_permissao_funcionario = reader.GetBoolean("pr_permissao_funcionario");
                dto.pr_permissao_cadastrar_funcionario = reader.GetBoolean("pr_permissao_cadastrar_funcionario");
                dto.pr_permissao_novo_funcionario = reader.GetBoolean("pr_permissao_novo_funcionario");
                dto.pr_permissao_alterar_dados_funcionarios = reader.GetBoolean("pr_permissao_alterar_dados_funcionarios");
                dto.pr_permissao_consultar_funcionarios = reader.GetBoolean("pr_permissao_consultar_funcionarios");
                dto.pr_permissao_fornecedor = reader.GetBoolean("pr_permissao_fornecedor");
                dto.pr_permissao_cadastrar_fornecedor = reader.GetBoolean("pr_permissao_cadastrar_fornecedor");
                dto.pr_permissao_consultar_fornecedor = reader.GetBoolean("pr_permissao_consultar_fornecedor");
                dto.pr_permissao_estoque = reader.GetBoolean("pr_permissao_estoque");
                dto.pr_permissao_consultar_estoque = reader.GetBoolean("pr_permissao_consultar_estoque");
                dto.pr_permissao_alterar_estoque = reader.GetBoolean("pr_permissao_alterar_estoque");
                dto.pr_permissao_perdir_ao_fornecedor = reader.GetBoolean("pr_permissao_perdir_ao_fornecedor");
                dto.pr_permissao_cadastrar_terceiriziados = reader.GetBoolean("pr_permissao_cadastrar_terceirizados");
                dto.pr_permissao_consultar_terceriziado = reader.GetBoolean("pr_permissao_consultar_terceriziado");
                dto.pr_permisssao_terceirizados = reader.GetBoolean("pr_permisssao_terceirizados");

             
            }
            reader.Close();
            return dto;
        }

    }
}
