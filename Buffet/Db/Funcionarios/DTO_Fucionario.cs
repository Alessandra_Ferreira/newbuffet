﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Funcionarios
{
    public class DTO_Fucionario
    {
        public int ID_Funcionario { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public DateTime Data_Cadastro { get; set; }
        public string Situacao { get; set; }
        public DateTime Data_Saida { get; set; }
        public string cargo { get; set; }
        public string observacao_Cadastro { get; set; }
        public string CEP { get; set; }
        public string UF { get; set; }
        public string cidade { get; set; }
        public string endereco { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string Numero { get; set; }
        public string observacoes_Endereco { get; set; }
        public string telefone { get; set; }
        public string celular { get; set; }
        public string email { get; set; }

        public string user_usuario { get; set; }
        public string sn_senha { get; set; }

        public bool pr_permissao_cliente { get; set; }
        public bool pr_permissao_cadastrar_cliente { get; set; }
        public bool pr_permissao_consultar_cliente { get; set; }
        public bool pr_permissao_atualiza_cadastror_cliente { get; set; }
        public bool pr_permissao_pedido { get; set; }
        public bool pr_permissao_novo_pedido { get; set; }
        public bool pr_permissao_atualizar_pedido { get; set; }
        public bool pr_permissao_consultar_pedido { get; set; }
        public bool pr_permissao_funcionario { get; set; }
        public bool pr_permissao_cadastrar_funcionario { get; set; }
        public bool pr_permissao_novo_funcionario { get; set; }
        public bool pr_permissao_alterar_dados_funcionarios { get; set; }
        public bool pr_permissao_consultar_funcionarios { get; set; }
        public bool pr_permissao_fornecedor { get; set; }
        public bool pr_permissao_cadastrar_fornecedor { get; set; }
        public bool pr_permissao_consultar_fornecedor { get; set; }
        public bool pr_permissao_estoque { get; set; }
        public bool pr_permissao_consultar_estoque { get; set; }
        public bool pr_permissao_alterar_estoque { get; set; }
        public bool pr_permissao_perdir_ao_fornecedor { get; set; }
        public bool pr_permissao_cadastrar_terceiriziados { get; set; }
        public bool pr_permissao_consultar_terceriziado { get; set; }
        public bool pr_permisssao_terceirizados { get; set; }

        public int fk_funcionario { get; set; }


    }
}
