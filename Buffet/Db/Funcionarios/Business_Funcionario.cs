﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Funcionarios
{
    class Business_Funcionario
    {

        public int Salvar(DTO_Fucionario dto)
        {


            if (dto.CPF == null)
            {
                throw new ArgumentException("O CPF é obrigatório!");
            }

            if (dto.Nome == null)
            {
                throw new ArgumentException("O nome é obrigatório!");
            }

            if (dto.Situacao == null)
            {
                throw new ArgumentException("O é obrigatório!");
            }

            if (dto.cargo == null)
            {
                throw new ArgumentException("O cargo é obrigatório!");
            }
            if (dto.endereco == null && dto.bairro == null && dto.CEP == null && dto.UF == null && dto.cidade == null && dto.Numero == null)
            {
                throw new ArgumentException("Os Campos de endereço são obrigatórios!");
            }
            if (dto.telefone == null && dto.celular == null && dto.email == null)
            {
                throw new ArgumentException("Os contatos são obrigatórios.");
            }
           

            Database_Funcionario db = new Database_Funcionario();
            int pk = db.Salvar(dto);
            return pk;
        }

        public DTO_Fucionario Logar (string usuario, string senha)
        {
            if (usuario == string.Empty)
            {
                throw new ArgumentException("O usuário é obrigatório.");
            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("A senha é obrigatória.");
            }
            Database_Funcionario db = new Database_Funcionario();
            return db.Logar(usuario, senha);
        }


        public List<DTO_Fucionario> Consultar(string nome)
        {
            Database_Funcionario db = new Database_Funcionario();
            return db.Consultar(nome);
        }
        public List<DTO_Fucionario> Listar()
        {
            Database_Funcionario db = new Database_Funcionario();
            return db.Listar();
        }

        public void Alterar (DTO_Fucionario dto)
        {
            Database_Funcionario db = new Database_Funcionario();
            db.Alterar(dto);
        }
    }
}

