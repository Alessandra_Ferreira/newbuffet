﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Terceirizados
{
    class Database_Terceirizados
    {
        Database db = new Database();

        public int Salvar(DTO_Terceirizados dto)
        {
            string script = @"INSERT INTO terceirizados 
            (                                         
                Nome_Fantasia,
                CPF_CNPJ,
                Situacao,
                Data_Cadastro,
                Data_Saida,
                Telefone1,
                Celular,
                Email,
                obsercacoes
            )
            VALUES 
            (
                @Nome_Fantasia,
                @CPF_CNPJ,
                @Situacao,
                @Data_Cadastro,
                @Data_Saida,
                @Telefone1,
                @Celular,
                @Email,
                @obsercacoes
            ) ";

            List< MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome_Fantasia", dto.nome_Fantasia));
            parms.Add(new MySqlParameter("CPF_CNPJ", dto.CPF_CNPJ));
            parms.Add(new MySqlParameter("Situacao", dto.Situacao));
            parms.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parms.Add(new MySqlParameter("Data_Saida", dto.Data_Saida));
            parms.Add(new MySqlParameter("Telefone1", dto.telefone));
            parms.Add(new MySqlParameter("Celular", dto.celular));
            parms.Add(new MySqlParameter("Email", dto.email));
            parms.Add(new MySqlParameter("obsercacoes", dto.observacoes));

            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        /*Listar é uma sobrecargar, caso o listar não tenha parametro irá
        *executar o primeiro Listar
        * caso tenha o parametro dto
        * irá executar o segundo
        */
        public List<DTO_Terceirizados> Listar()
        {
            string script = @"Select * from terceirizados";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Terceirizados> Terceiros = new List<DTO_Terceirizados>();

            while(reader.Read())
            {
                DTO_Terceirizados dto = new DTO_Terceirizados();
                dto.ID = reader.GetInt32("ID_Terceiros");
                dto.nome_Fantasia = reader.GetString("Nome_Fantasia");
                dto.CPF_CNPJ = reader.GetString("CPF_CNPJ");
                dto.Situacao = reader.GetString("Situacao");
                dto.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                dto.Data_Saida = reader.GetDateTime("Data_Saida");
                dto.telefone = reader.GetString("Telefone1");
                dto.celular = reader.GetString("Celular");
                dto.email = reader.GetString("Email");
                dto.observacoes = reader.GetString("obsercacoes");

                Terceiros.Add(dto);
            }

            reader.Close();
            return Terceiros;
        }

        public List<DTO_Terceirizados> Consultar(DTO_Terceirizados dto)
        {
            string script = @"Select * from terceirizados WHERE Nome_Fantasia LIKE @Nome_Fantasia";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome_Fantasia", "%" + dto.nome_Fantasia + "%"));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Terceirizados> Terceiros = new List<DTO_Terceirizados>();

            while (reader.Read())
            {
                DTO_Terceirizados info = new DTO_Terceirizados();
                info.ID = reader.GetInt32("ID_Terceiros");
                info.nome_Fantasia = reader.GetString("Nome_Fantasia");
                info.CPF_CNPJ = reader.GetString("CPF_CNPJ");
                info.Situacao = reader.GetString("Situacao");
                info.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                info.Data_Saida = reader.GetDateTime("Data_Saida");
                info.telefone = reader.GetString("Telefone1");
                info.celular = reader.GetString("Celular");
                info.email = reader.GetString("Email");
                info.observacoes = reader.GetString("obsercacoes");

                Terceiros.Add(info);
            }

            reader.Close();
            return Terceiros;
        }

        public void Alterar(DTO_Terceirizados dto)
        {
            string script = @"UPDATE terceirizados SET 
                                          Nome_Fantasia = @Nome_Fantasia,
                                          CPF_CNPJ = @CPF_CNPJ,
                                          Situacao = @Situacao,
                                          Data_Cadastro = @Data_Cadastro,
                                          Data_Saida = @Data_Saida,
                                          Telefone1 = @Telefone1,
                                          Celular = @Celular,
                                          Email = @Email,
                                          obsercacoes = @obsercacoes
                               WHERE ID_Terceiros = @ID_Terceiros";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            //Faltando declarar o ID
            parms.Add(new MySqlParameter("ID_Terceiros", dto.ID));
            parms.Add(new MySqlParameter("Nome_Fantasia", dto.nome_Fantasia));
            parms.Add(new MySqlParameter("CPF_CNPJ", dto.CPF_CNPJ));
            parms.Add(new MySqlParameter("Situacao", dto.Situacao));
            parms.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parms.Add(new MySqlParameter("Data_Saida", dto.Data_Saida));
            parms.Add(new MySqlParameter("Telefone1", dto.telefone));
            parms.Add(new MySqlParameter("Celular", dto.celular));
            parms.Add(new MySqlParameter("Email", dto.email));
            parms.Add(new MySqlParameter("obsercacoes", dto.observacoes));

            db.ExecuteInsertScript(script,parms);
        }

        public void Remover(DTO_Terceirizados dto)
        {
            string script = @"DELETE FROM terceirizados 
                                    WHERE ID_Terceiros = @ID_Terceiros";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Terceiros",dto.ID));

            db.ExecuteInsertScript(script, parms);
        }
    }
}
