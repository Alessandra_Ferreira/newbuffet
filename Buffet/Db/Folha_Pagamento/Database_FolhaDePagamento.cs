﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.FolhaDePagamento
{
    class Database_FolhaDePagamento
    {

        Database db = new Database();

        public int Salvar(DTO_FolhaDePagamento dto)
        {
            string script =
                @"INSERT INTO 
                   tb_folha_de_pagamento(      
                    sl_salario_mes,
                    vl_vale_trasporte,
                    vl_vale_refeicao,
                    ad_adiantamento,
                    inss,
                    hr_hora_extra,
                    ds_descontos,
                    sl_salario_base,
                    cl_calculo_base_inss,
                    cl_cauculo_base_fgts,
                    ft_fgts_do_mes,
                    bs_base_calc_irrf,
                    fi_faixa_irrf,
                    tl_total_vencimentos,
                    rf_referencia,
                    ms_mensagem,
                    sl_salario_bruto,
                    pl_plano_de_saude,
                    hr_horas_trabalhadas,
                    sl_hora,
                    dias_trabalhados,
                    dt_datapagamento,
                    fk_id_funcionario_fp,
                    fk_id_empresa_fp
                    )
                    VALUES
                    (       
                    @sl_salario_mes,
                    @vl_vale_trasporte,
                    @vl_vale_refeicao,
                    @ad_adiantamento,
                    @inss,
                    @hr_hora_extra,
                    @ds_descontos,
                    @sl_salario_base,
                    @cl_calculo_base_inss,
                    @cl_cauculo_base_fgts,
                    @ft_fgts_do_mes,
                    @bs_base_calc_irrf,
                    @fi_faixa_irrf,
                    @tl_total_vencimentos,
                    @rf_referencia,
                    @ms_mensagem,
                    @sl_salario_bruto,
                    @pl_plano_de_saude,
                    @hr_horas_trabalhadas,
                    @sl_hora,
                    @dias_trabalhados,
                    @dt_datapagamento,
                    @fk_id_funcionario_fp,
                    @fk_id_empresa_fp)";

            List<MySqlParameter> parm = new List<MySqlParameter>();

            parm.Add(new MySqlParameter("sl_salario_mes", dto.sl_salario_mes ));
            parm.Add(new MySqlParameter("vl_vale_trasporte", dto.vl_vale_trasporte ));
            parm.Add(new MySqlParameter("vl_vale_refeicao", dto.vl_vale_refeicao));
            parm.Add(new MySqlParameter("ad_adiantamento", dto.ad_adiantamento));
            parm.Add(new MySqlParameter("inss", dto.inss ));
            parm.Add(new MySqlParameter("hr_hora_extra", dto.hr_hora_extra ));
            parm.Add(new MySqlParameter("ds_descontos", dto.ds_descontos));
            parm.Add(new MySqlParameter("sl_salario_base", dto.sl_salario_base));
            parm.Add(new MySqlParameter("cl_calculo_base_inss", dto.cl_calculo_base_inss));
            parm.Add(new MySqlParameter("cl_cauculo_base_fgts", dto.cl_cauculo_base_fgts));
            parm.Add(new MySqlParameter("ft_fgts_do_mes", dto.ft_fgts_do_mes));
            parm.Add(new MySqlParameter("bs_base_calc_irrf",dto.bs_base_calc_irrf ));
            parm.Add(new MySqlParameter("fi_faixa_irrf", dto.fi_faixa_irrf));
            parm.Add(new MySqlParameter("tl_total_vencimentos", dto.tl_total_vencimentos ));
            parm.Add(new MySqlParameter("rf_referencia", dto.rf_referencia ));
            parm.Add(new MySqlParameter("ms_mensagem", dto.ms_mensagem ));
            parm.Add(new MySqlParameter("sl_salario_bruto", dto.sl_salario_bruto ));
            parm.Add(new MySqlParameter("pl_plano_de_saude", dto.pl_plano_de_saude ));
            parm.Add(new MySqlParameter("hr_horas_trabalhadas", dto.hr_horas_trabalhadas ));
            parm.Add(new MySqlParameter("sl_hora", dto.sl_hora));
            parm.Add(new MySqlParameter("dias_trabalhados", dto.dias_trabalhados ));
            parm.Add(new MySqlParameter("dt_datapagamento", dto.dt_datapagamento));
            parm.Add(new MySqlParameter("fk_id_funcionario_fp", dto.fk_id_funcionario_fp ));
            parm.Add(new MySqlParameter("fk_id_empresa_fp", dto.fk_id_empresa_fp));


            return db.ExecuteInsertScriptWithPk(script, parm);
            
        }

        public void Remover(DTO_FolhaDePagamento dto)
        {
            string script = @"DELETE FROM tb_folha_de_pagamento WHERE id_folha_de_pagamento = @id_folha_de_pagamento";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_folha_de_pagamento", dto.id_folha_de_pagamento));

            db.ExecuteSelectScript(script, parms);
        }

        public List<DTO_FolhaDePagamento>Listar(DTO_FolhaDePagamento dto)
        {
            string script = @"Select * from tb_folha_de_pagamento";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_FolhaDePagamento> folhapagamento = new List<DTO_FolhaDePagamento>();

            while (reader.Read())
            {              
                DTO_FolhaDePagamento dtos = new DTO_FolhaDePagamento();
                dtos.sl_salario_mes = reader.GetDouble("sl_salario_mes");
                dtos.vl_vale_trasporte = reader.GetDouble("vl_vale_trasporte");
                dtos.vl_vale_refeicao = reader.GetDouble("vl_vale_refeicao");
                dtos.ad_adiantamento = reader.GetDouble("ad_adiantamento");
                dtos.inss = reader.GetDouble("inss");
                dtos.hr_hora_extra = reader.GetDateTime("hr_hora_extra");
                dtos.ds_descontos = reader.GetDouble("ds_descontos");
                dtos.sl_salario_base = reader.GetDouble("sl_salario_base");
                dtos.cl_calculo_base_inss = reader.GetDouble("cl_calculo_base_inss");
                dtos.cl_cauculo_base_fgts = reader.GetDouble("cl_cauculo_base_fgts");
                dtos.ft_fgts_do_mes = reader.GetDouble("ft_fgts_do_mes");
                dtos.bs_base_calc_irrf = reader.GetDouble("bs_base_calc_irrf");
                dtos.fi_faixa_irrf = reader.GetDouble("fi_faixa_irrf");
                dtos.tl_total_vencimentos = reader.GetDouble("tl_total_vencimentos");
                dtos.rf_referencia = reader.GetDouble("rf_referencia");
                dtos.ms_mensagem = reader.GetString("ms_mensagem");
                dtos.sl_salario_bruto = reader.GetDouble("sl_salario_bruto");
                dtos.pl_plano_de_saude = reader.GetDouble("pl_plano_de_saude");
                dtos.hr_horas_trabalhadas = reader.GetDateTime("hr_horas_trabalhadas");
                dtos.sl_hora = reader.GetDouble("sl_hora");
                dtos.dias_trabalhados = reader.GetDateTime("dias_trabalhados");
                dtos.dt_datapagamento = reader.GetDateTime("dt_datapagamento");
                dtos.fk_id_funcionario_fp = reader.GetInt32("fk_id_funcionario_fp");
                dtos.fk_id_empresa_fp = reader.GetInt32("fk_id_empresa_fp");


                folhapagamento.Add(dtos);
            }
            reader.Close();

                return folhapagamento;
        }
        //public void Alterar(DTO_FolhaDePagamento dto)
        //{
        //    string script = @"";
        //    List<MySqlParameter> parm = new List<MySqlParameter>();
        //    parm.Add(new MySqlParameter("",));

        //    Database db = new Database();
        //    db.ExecuteInsertScriptWithPk(script, parm);
        //}
    }
}
