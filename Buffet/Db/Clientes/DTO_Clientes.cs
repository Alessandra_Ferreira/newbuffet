﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Clientes
{
    public class DTO_Clientes
    {
       public int ID { get; set; }
       public string Nome { get; set; }
        public string Celular { get; set; }
        public string CPF { get; set; }
        public DateTime Data_Nascimento { get; set; }
        public DateTime Data_Cadastro { get; set; }
        public string Observacao_Cadastro { get; set; }
        public string CEP { get; set; }
        public string UF { get; set; }
        public string Cidade { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Numero { get; set; }
        public string Observacoes_Endereco { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }









    }
}
