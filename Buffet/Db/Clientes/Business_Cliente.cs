﻿using Buffet.Db.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Clientes
{
   public  class Business_Cliente
    {
        public int Salvar(DTO_Clientes dto)
        {
            if(dto.CPF == null)
            {
                throw new ArgumentException("O CPF é obrigatório!");
            }
            if(dto.Nome == null)
            {
                throw new ArgumentException("O Nome é obrigatório!");
            }
            if(dto.Data_Nascimento == null)
            {
                throw new ArgumentException("A data de nascimento é obrigatória!");
            }


            Database_Cliente db = new Database_Cliente();
            int pk = db.Salvar(dto);
            return pk;
        }

        public List<DTO_Clientes> Consultar(DTO_Clientes dto)
        {
            Database_Cliente db = new Database_Cliente();
            List<DTO_Clientes> consultar = db.Consultar(dto);

            return consultar;
        }

        public void Alterar(DTO_Clientes dto)
        {
            Database_Cliente db = new Database_Cliente();
            db.Alterar(dto);
        }

        public void Remover(DTO_Clientes dto)
        {
            Database_Cliente db = new Database_Cliente();
            db.Remover(dto);
        }
        public DTO_Clientes Consultar(string cpf)
        {
            Database_Cliente db = new Database_Cliente();
            DTO_Clientes consultar = db.Consultar(cpf);

            return consultar;
        }
    }
}

       

