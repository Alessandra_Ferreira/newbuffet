﻿using Buffet.Db.Base;
using Buffet.Db.Clientes;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Clientes
{
    class Database_Cliente
    {
        public int Salvar(DTO_Clientes dto)
        {

            string script = @"Insert into cliente 
            (
                Nome,
                CPF,
                DataNascimento,
                Data_Cadastro,
                observacao_Cadastro,
                CEP,
                UF,
                Cidade,
                Endereco,
                Complemento,
                Bairro,
                Numero,
                Observacoes_Endereco,
                Telefone,
                Celular,
                Email
            ) 
            VALUES
            (
                @Nome,
                @CPF,
                @DataNascimento,
                @Data_Cadastro,
                @observacao_Cadastro,
                @CEP,
                @UF,
                @Cidade,
                @Endereco,
                @Complemento,
                @Bairro,
                @Numero,
                @Observacoes_Endereco,
                @Telefone,
                @Celular,
                @Email
             )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("DataNascimento", dto.Data_Nascimento));
            parms.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parms.Add(new MySqlParameter("observacao_Cadastro", dto.Observacao_Cadastro));
            parms.Add(new MySqlParameter("CEP", dto.CEP));
            parms.Add(new MySqlParameter("UF", dto.UF));
            parms.Add(new MySqlParameter("Cidade", dto.Cidade));
            parms.Add(new MySqlParameter("Endereco", dto.Endereco));
            parms.Add(new MySqlParameter("Complemento", dto.Complemento));
            parms.Add(new MySqlParameter("Bairro", dto.Bairro));
            parms.Add(new MySqlParameter("Numero", dto.Numero));
            parms.Add(new MySqlParameter("Observacoes_Endereco", dto.Observacoes_Endereco));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("Celular", dto.Celular));
            parms.Add(new MySqlParameter("Email", dto.Email));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);         

        }

        public void Remover(DTO_Clientes dto) {
            string script = @"DELETE FROM  cliente WHERE ID_Cliente = @ID_cliente";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Cliente", dto.ID));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }

        public void Alterar(DTO_Clientes dto )
        {
            string script = @"Update cliente 
                                 SET Nome                 =  @Nome,
                                     CPF                  =  @CPF,
                                     DataNascimento       =  @DataNascimento,
                                     Data_Cadastro        =  @Data_Cadastro,
                                     observacao_Cadastro  =  @observacao_Cadastro,
                                     CEP                  =  @CEP,
                                     UF                   =  @UF,
                                     Cidade               =  @Cidade,
                                     Endereco             =  @Endereco,
                                     Complemento          =  @Complemento,
                                     Bairro               =  @Bairro,
                                     Numero               =  @Numero,
                                     Observacoes_Endereco =  @Observacoes_Endereco,
                                     Telefone             =  @Telefone,
                                     Celular              =  @Celular,
                                     Email                =  @Email
                               WHERE ID_Cliente           =  @ID_Cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Cliente", dto.ID));
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("DataNascimento", dto.Data_Nascimento));
            parms.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parms.Add(new MySqlParameter("observacao_Cadastro", dto.Observacao_Cadastro));
            parms.Add(new MySqlParameter("CEP", dto.CEP));
            parms.Add(new MySqlParameter("UF", dto.UF));
            parms.Add(new MySqlParameter("Cidade", dto.Cidade));
            parms.Add(new MySqlParameter("Endereco", dto.Endereco));
            parms.Add(new MySqlParameter("Complemento", dto.Complemento));
            parms.Add(new MySqlParameter("Bairro", dto.Bairro));
            parms.Add(new MySqlParameter("Numero", dto.Numero));
            parms.Add(new MySqlParameter("Observacoes_Endereco", dto.Observacoes_Endereco));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("Celular", dto.Celular));
            parms.Add(new MySqlParameter("Email", dto.Email));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }

        public List<DTO_Clientes> Consultar(DTO_Clientes dto)
        {
            string script = @"SELECT * FROM cliente WHERE Nome like @Nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Clientes> lista = new List<DTO_Clientes>();
            while (reader.Read())
            {
                DTO_Clientes cli = new DTO_Clientes();
                cli.ID = reader.GetInt32("ID_Cliente");
                cli.Nome = reader.GetString("Nome");
                cli.CPF = reader.GetString("CPF");
                cli.Data_Nascimento = reader.GetDateTime("DataNascimento");
                cli.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                cli.Observacao_Cadastro = reader.GetString("observacao_Cadastro");
                cli.CEP = reader.GetString("CEP");
                cli.UF = reader.GetString("UF");
                cli.Cidade = reader.GetString("Cidade");
                cli.Endereco = reader.GetString("Endereco");
                cli.Complemento = reader.GetString("Complemento");
                cli.Bairro = reader.GetString("Bairro");
                cli.Numero = reader.GetString("Numero");
                cli.Observacoes_Endereco = reader.GetString("Observacoes_Endereco");
                cli.Telefone = reader.GetString("Telefone");
                cli.Celular = reader.GetString("Celular");
                cli.Email = reader.GetString("Email");
                
                lista.Add(cli);
            }
            reader.Close();
            return lista;
        }
        public DTO_Clientes Consultar(string cpf)
        {
            string script = @"SELECT * FROM cliente WHERE CPF like @CPF";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("CPF", "%" + cpf + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            DTO_Clientes cli = new DTO_Clientes(); 

            while (reader.Read())
            {
                
                cli.ID = reader.GetInt32("ID_Cliente");
                cli.Nome = reader.GetString("Nome");
                cli.CPF = reader.GetString("CPF");
                cli.Data_Nascimento = reader.GetDateTime("DataNascimento");
                cli.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                cli.Observacao_Cadastro = reader.GetString("observacao_Cadastro");
                cli.CEP = reader.GetString("CEP");
                cli.UF = reader.GetString("UF");
                cli.Cidade = reader.GetString("Cidade");
                cli.Endereco = reader.GetString("Endereco");
                cli.Complemento = reader.GetString("Complemento");
                cli.Bairro = reader.GetString("Bairro");
                cli.Numero = reader.GetString("Numero");
                cli.Observacoes_Endereco = reader.GetString("Observacoes_Endereco");
                cli.Telefone = reader.GetString("Telefone");
                cli.Celular = reader.GetString("Celular");
                cli.Email = reader.GetString("Email");
                              
                
            }
            reader.Close();
            return cli;
        }
       
    }
}
