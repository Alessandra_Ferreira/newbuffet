﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Agenda
{
    class Business_agenda
    {

        //public int ID_agenda { get; set; }
        //public string dt_diasemana { get; set; }
        //public string hr_horario { get; set; }
        //public string obs_observacao { get; set; }
        //public DateTime dt_datareserva { get; set; }
        //public DateTime dt_dataatual { get; set; }
        public int Salvar(DTO_agenda dto)
        {
            if (dto.dt_diasemana == null)
            {
                throw new ArgumentException("O dia da semana é obrigatorio!");
            }
            if (dto.hr_horario == null)
            {
                throw new ArgumentException("O horario é obrigatorio!");
            }
            if (dto.obs_observacao == null  )
            {

            }
            Database_agenda database_Agenda = new Database_agenda();
            int pk = database_Agenda.Salvar(dto);
            return pk;
        }
    }
}
