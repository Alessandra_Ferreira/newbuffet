﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Agenda
{
    class Database_agenda
    {

        public int Salvar(DTO_agenda dto)
        {
            string script = @"INSERT INTO agenda (da_dia, 
                                                hr_hora,
                                                dt_data, 
                                                obs_observacao, 
                                                dt_dia_do_agendamento ) 
                                                VALUES
                                                (@da_dia, 
                                                @hr_hora,
                                                @dt_data, 
                                                @obs_observacao, 
                                                @dt_dia_do_agendamento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("da_dia", dto.dt_diasemana));
            parms.Add(new MySqlParameter("hr_hora", dto.hr_horario));
            parms.Add(new MySqlParameter("dt_data", dto.dt_datareserva));
            parms.Add(new MySqlParameter("obs_observacao", dto.obs_observacao));
            parms.Add(new MySqlParameter("dt_dia_do_agendamento", dto.dt_dataatual));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

    }
}
