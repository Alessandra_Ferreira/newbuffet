﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Estoque
{
    public class Database_Estoque
    {
        Database db = new Database();


        public int Salvar(DTO_Estoque dto)
        {

            string script = @"INSERT INTO 
            estoque (
            produto,
             Qtd_Entrada,
             Data_Entrada,
             Qtd_Saida,
             data_saida,
             Qtd_Atual,
             Data_Atual,
             Data_Validade)
            Values(
             @produto,
             @Qtd_Entrada,
             @Data_Entrada,
             @Qtd_Saida,
             @data_saida,
             @Qtd_Atual,
             @Data_Atual,
             @Data_Validade)";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("produto", dto.Produto));
            parm.Add(new MySqlParameter("Qtd_Entrada", dto.Quantidade_Entrada));
            parm.Add(new MySqlParameter("Data_Entrada", dto.Data_Entrada));
            parm.Add(new MySqlParameter("Qtd_Saida", dto.Quantidade_Saida));
            parm.Add(new MySqlParameter("data_saida", dto.Data_Saida));
            parm.Add(new MySqlParameter("Qtd_Atual", dto.Quantidae_Atual));
            parm.Add(new MySqlParameter("Data_Atual", dto.Data_Atual));
            parm.Add(new MySqlParameter("Data_Validade", dto.Data_Validade));

            return db.ExecuteInsertScriptWithPk(script, parm);
            }

        public void Remover(DTO_Estoque dto)
        {
            string script = @"Delete from estoque where id_estoque = @id_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque",dto.id));
       
            db.ExecuteSelectScript(script,parms);


        }

        public void Alterar(DTO_Estoque dto) {
          
            string script = @"UPDATE estoque SET
             produto = @produto,
             Qtd_Entrada = @Qtd_Entrada,
             Data_Entrada = @Data_Entrada ,
             Qtd_Saida = @Qtd_Saida ,
             data_saida = @data_saida,
             Qtd_Atual = @Qtd_Atual,
             Data_Atual = @Data_Atual,
             Data_Validade = @Data_Validade
             ";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("produto", dto.Produto));
            parm.Add(new MySqlParameter("Qtd_Entrada", dto.Quantidade_Entrada));
            parm.Add(new MySqlParameter("Data_Entrada", dto.Data_Entrada));
            parm.Add(new MySqlParameter("Qtd_Saida", dto.Quantidade_Saida));
            parm.Add(new MySqlParameter("data_saida", dto.Data_Saida));
            parm.Add(new MySqlParameter("Qtd_Atual", dto.Quantidae_Atual));
            parm.Add(new MySqlParameter("Data_Atual", dto.Data_Atual));
            parm.Add(new MySqlParameter("Data_Validade", dto.Data_Validade));

            Database db = new Database();
             db.ExecuteInsertScriptWithPk(script, parm);

        }

        public List<DTO_Estoque> Lista()
        {
            string script = @"Select * from estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Estoque> estoque = new List<DTO_Estoque>();

            while (reader.Read())
            {
                DTO_Estoque dto = new DTO_Estoque();
                dto.id = reader.GetInt32("id");
                dto.Produto = reader.GetString("produto");
                dto.Quantidade_Entrada = reader.GetInt32("Qtd_Entrada");
                dto.Data_Entrada = reader.GetDateTime("Data_Entrada");
                dto.Quantidade_Saida = reader.GetInt32("Qtd_Saida");
                dto.Data_Saida = reader.GetDateTime("data_saida");
                dto.Quantidae_Atual = reader.GetInt32("Qtd_Atual");

                dto.Data_Validade = reader.GetDateTime("Data_Validade");


                estoque.Add(dto);
            }
            reader.Close();

            return estoque;
        }
        public List<DTO_Estoque> Lista(string produto)
        {
            string script = @"Select * from estoque WHERE produto = @produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("produto", produto));
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Estoque> estoque = new List<DTO_Estoque>();

            while (reader.Read())
            {
                DTO_Estoque dto = new DTO_Estoque();
                dto.id = reader.GetInt32("id");
                dto.Produto = reader.GetString("produto");
                dto.Quantidade_Entrada = reader.GetInt32("Qtd_Entrada");
                dto.Data_Entrada = reader.GetDateTime("Data_Entrada");
                dto.Quantidade_Saida = reader.GetInt32("Qtd_Saida");
                dto.Data_Saida = reader.GetDateTime("data_saida");
                dto.Quantidae_Atual = reader.GetInt32("Qtd_Atual");

                dto.Data_Validade = reader.GetDateTime("Data_Validade");


                estoque.Add(dto);
            }
            reader.Close();

            return estoque;
        }
    }
}
