﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Fornecedores
{
    class Database_Fornecedores
    {
       public int Salvar(DTO_Fornecedores dto)
        {
            string script =
            @"INSERT INTO fornecedores
            (   Nome_Fantasia,
                CPF_CNPJ,
                Situacao,
                Data_Cadastro,
                Data_Saida,
                Observacoes_Cadastro,
                Endereco,
                Bairro,
                Numero,
                Complemento,
                Cidade,
                UF,
                CEP,
                Obervacoes_Endereco,
                Telefone,
                Celular,
                Email
            )
            VALUES 
            (   
                @Nome_Fantasia,
                @CPF_CNPJ,
                @Situacao,
                @Data_Cadastro,
                @Data_Saida,
                @Observacoes_Cadastro,
                @Endereco,
                @Bairro,
                @Numero,
                @Complemento,
                @Cidade,
                @UF,
                @CEP,
                @Obervacoes_Endereco,
                @Telefone,
                @Celular,
                @Email
             ) ";

            List<MySqlParameter> parm = new List<MySqlParameter>();
           
            parm.Add(new MySqlParameter("Nome_Fantasia", dto.Nome_Fantasia));
            parm.Add(new MySqlParameter("CPF_CNPJ", dto.CPF_CNPJ));
            parm.Add(new MySqlParameter("Situacao", dto.Situacao));
            parm.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parm.Add(new MySqlParameter("Data_Saida", dto.Data_Saida));
            parm.Add(new MySqlParameter("Observacoes_Cadastro", dto.Observacoes_Endereco));
            parm.Add(new MySqlParameter("Endereco", dto.Endereco));
            parm.Add(new MySqlParameter("Bairro", dto.Bairro));
            parm.Add(new MySqlParameter("Numero", dto.Numero));
            parm.Add(new MySqlParameter("Complemento", dto.Complemento));
            parm.Add(new MySqlParameter("Cidade", dto.Cidade));
            parm.Add(new MySqlParameter("UF", dto.UF));
            parm.Add(new MySqlParameter("CEP", dto.CEP));
            parm.Add(new MySqlParameter("Obervacoes_Endereco", dto.Observacoes_Endereco));
            parm.Add(new MySqlParameter("Telefone", dto.Telefone));
            parm.Add(new MySqlParameter("Celular", dto.Celular));
            parm.Add(new MySqlParameter("Email", dto.Email));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public List<DTO_Fornecedores> Consultar (DTO_Fornecedores dto)
        {
            string script =
                @"SELECT * FROM fornecedores
                          WHERE Nome_Fantasia   LIKE @Nome_Fantasia";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("Nome_Fantasia", "%" + dto.Nome_Fantasia + "%"));
            
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Fornecedores> fora = new List<DTO_Fornecedores>();

            while (reader.Read())
            {
                DTO_Fornecedores dentro = new DTO_Fornecedores();
                dentro.ID_Fornecedor = reader.GetInt32("ID_fornecedores");
                dentro.Bairro = reader.GetString("Bairro");
                dentro.Celular = reader.GetString("Celular");
                dentro.CEP = reader.GetString("CEP");
                dentro.Cidade = reader.GetString("Cidade");
                dentro.Complemento = reader.GetString("Complemento");
                dentro.CPF_CNPJ = reader.GetString("CPF_CNPJ");
                dentro.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                dentro.Data_Saida = reader.GetDateTime("Data_Saida");
                dentro.Email = reader.GetString("Email");
                dentro.Endereco = reader.GetString("Endereco");
                dentro.Nome_Fantasia = reader.GetString("Nome_Fantasia");
                dentro.Numero = reader.GetString("Numero");
                dentro.observacao_Cadastro = reader.GetString("Observacoes_Cadastro");
                dentro.Observacoes_Endereco = reader.GetString("Obervacoes_Endereco");
                dentro.Situacao = reader.GetString("Situacao");
                dentro.Telefone = reader.GetString("Telefone");
                dentro.UF = reader.GetString("UF");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }

        public List<DTO_Fornecedores> List()
        {
            string script = @"Select * from fornecedores";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            List<DTO_Fornecedores> lista = new List<DTO_Fornecedores>();
            while (reader.Read())
            {
                DTO_Fornecedores dto = new DTO_Fornecedores();
                dto.ID_Fornecedor = reader.GetInt32("ID_fornecedores");
                dto.Bairro = reader.GetString("Bairro");
                dto.Celular = reader.GetString("Celular");
                dto.CEP = reader.GetString("CEP");
                dto.Cidade = reader.GetString("Cidade");
                dto.Complemento = reader.GetString("Complemento");
                dto.CPF_CNPJ = reader.GetString("CPF_CNPJ");
                dto.Data_Cadastro = reader.GetDateTime("Data_Cadastro");
                dto.Data_Saida = reader.GetDateTime("Data_Saida");
                dto.Email = reader.GetString("Email");
                dto.Endereco = reader.GetString("Endereco");
                dto.Nome_Fantasia = reader.GetString("Nome_Fantasia");
                dto.Numero = reader.GetString("Numero");
                dto.observacao_Cadastro = reader.GetString("Observacoes_Cadastro");
                dto.Observacoes_Endereco = reader.GetString("Obervacoes_Endereco");
                dto.Situacao = reader.GetString("Situacao");
                dto.Telefone = reader.GetString("Telefone");
                dto.UF = reader.GetString("UF");

                lista.Add(dto);

            }
            reader.Close();
            return lista;                          

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM fornecedores 
                                    WHERE ID_Fornecedores = @ID_Fornecedores";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ID_Fornecedores", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar (DTO_Fornecedores dto)
        {
            string script = @"Update fornecedores SET
                                            Nome_Fantasia = @Nome_Fantasia,
                                            CPF_CNPJ = @CPF_CNPJ,
                                            Situacao = @Situacao,
                                            Data_Cadastro = @Data_Cadastro,
                                            Data_Saida = @Data_Saida,
                                            Observacoes_Cadastro = @Observacoes_Cadastro,
                                            Endereco = @Endereco,
                                            Bairro = @Bairro,
                                            Numero= @Numero,
                                            Complemento = @Complemento,
                                            Cidade = @Cidade,
                                            UF = @UF,
                                            CEP = @CEP,
                                            Obervacoes_Endereco = @Obervacoes_Endereco,
                                            Telefone = @Telefone,
                                            Celular = @Celular,
                                            Email = @Email  
                                    WHERE ID_Fornecedores = @ID_Fornecedores";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ID_fornecedores", dto.ID_Fornecedor));
            parm.Add(new MySqlParameter("Nome_Fantasia", dto.Nome_Fantasia));
            parm.Add(new MySqlParameter("CPF_CNPJ", dto.CPF_CNPJ));
            parm.Add(new MySqlParameter("Situacao", dto.Situacao));
            parm.Add(new MySqlParameter("Data_Cadastro", dto.Data_Cadastro));
            parm.Add(new MySqlParameter(" Data_Saida", dto.Data_Saida));
            parm.Add(new MySqlParameter("Observacoes_Cadastro", dto.Observacoes_Endereco));
            parm.Add(new MySqlParameter("Endereco", dto.Endereco));
            parm.Add(new MySqlParameter("Bairro", dto.Bairro));
            parm.Add(new MySqlParameter("Numero", dto.Numero));
            parm.Add(new MySqlParameter("Complemento", dto.Complemento));
            parm.Add(new MySqlParameter("Cidade", dto.Cidade));
            parm.Add(new MySqlParameter("UF", dto.UF));
            parm.Add(new MySqlParameter("CEP", dto.CEP));
            parm.Add(new MySqlParameter("Obervacoes_Endereco", dto.Observacoes_Endereco));
            parm.Add(new MySqlParameter("Telefone", dto.Telefone));
            parm.Add(new MySqlParameter("Celular", dto.Celular));
            parm.Add(new MySqlParameter("Email", dto.Email));


            Database db = new Database();
            db.ExecuteInsertScript(script, parm);

        }


    }
}