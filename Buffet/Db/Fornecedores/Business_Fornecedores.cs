﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Fornecedores
{
    class Business_Fornecedores
    {
            
        public int Salvar (DTO_Fornecedores dto)
        {

            if (dto.Nome_Fantasia == null)
            {
                throw new ArgumentException("O Nome é obrigatório!");
            }
            if (dto.CPF_CNPJ == null)
            {
                throw new ArgumentException("O CNPJ é obrigatório!");
            }
            if (dto.Situacao == null)
            {
                throw new ArgumentException("Campo obrigatória.");
            }
            if (dto.Telefone == string.Empty && dto.Celular == string.Empty && dto.Email == string.Empty)
            {
                throw new ArgumentException("Os contatos são obrigatórios.");
            }
            if (dto.Endereco == null && dto.Bairro == null && dto.CEP == null && dto.UF == null && dto.Cidade == null && dto.Numero == null)
            {
                throw new ArgumentException("Os Campos de endereço são obrigatórios!");
            }
           

            Database_Fornecedores database_Fornecedores = new Database_Fornecedores();

            int pk = database_Fornecedores.Salvar(dto);
            return pk;
        }

        public void Alterar(DTO_Fornecedores dto)
        {
            Database_Fornecedores database_Fornecedores = new Database_Fornecedores();

            database_Fornecedores.Alterar(dto);
        }

        public void Remover(int id)
        {
            Database_Fornecedores database_Fornecedores = new Database_Fornecedores();

            database_Fornecedores.Remover(id);
        }

        public List<DTO_Fornecedores> Listar()
        {
            Database_Fornecedores database_Fornecedores = new Database_Fornecedores();

            List<DTO_Fornecedores> lista = database_Fornecedores.List();
            return lista;
        }
        public List<DTO_Fornecedores> Consultar(DTO_Fornecedores dto)
        {
            Database_Fornecedores database_Fornecedores = new Database_Fornecedores();
            List<DTO_Fornecedores> consultar = database_Fornecedores.Consultar(dto);

            return consultar;
        }
    }
}
