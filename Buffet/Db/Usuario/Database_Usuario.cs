﻿using Buffet.Db.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buffet.Db.Usuario
{
    class Database_Usuario
    {
        public int Salvar(DTO_Usuario dto)
        {
            string script = @"INSERT INTO tb_Usuario (Login, Senha) VALUES ( @Login, @Senha)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Login", dto.Login));
            parms.Add(new MySqlParameter("Senha", dto.senha));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }
}
